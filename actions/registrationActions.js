export const SET_PHONE_NUMBER = 'SET_PHONE_NUMBER';
export const REGISTRATION_COMPLETE = 'REGISTRATION_COMPLETE';
export const REGISTRATION_NOTIFICATION = 'REGISTRATION_NOTIFICATION';
export const REGISTRATION_NOT_COMPLETE = 'REGISTRATION_NOT_COMPLETE'


export function setPhoneNumber(phoneNumber) {
  return (dispatch) => {
    dispatch({ type: SET_PHONE_NUMBER, phoneNumber });
  };
}

export function setRegistrationComplete() {
  return (dispatch) => {
    dispatch({ type: REGISTRATION_COMPLETE })
  }
}

export function sendNotificationRegistrationSuccess() {
  return (dispatch) => {
    dispatch({ type: REGISTRATION_NOTIFICATION })
  }
}


export function checkIsRegistered(isRegistered,phoneNumber) {
  console.log('CheckIsRegistered IS RUNNING ................................')
  return (dispatch) => {
    fetch(`http://rafshadow.ir/GetRegistrationStatus?phone=${phoneNumber}&serviceId=608`)
    .then((response)=> response.json())
    .then((res)=>{
      console.log(`RegistrationStatus For ${phoneNumber} is ::::::::`,res)
      console.log(`res.RegistrationStatus[0].Status For ${phoneNumber} is ::::::::`,typeof res.RegistrationStatus[0].Status)
      if(res.RegistrationStatus[0].Status === "Unregistered"){
        dispatch({ type: REGISTRATION_NOT_COMPLETE })
      }
    })
  }
}