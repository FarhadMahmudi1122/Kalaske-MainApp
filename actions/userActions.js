export const ALERT_ACTION = 'ALERT_ACTION';
export const SET_DATE_FOR_CHILDBIRTH_LOGIN_TIRTH = 'SET_DATE_FOR_CHILDBIRTH_LOGIN_TIRTH';
export const SET_USER_INFO_LOGIN_FORTH = 'SET_USER_INFO_LOGIN_FORTH';
export const SET_MOTHER_NAME_MOTHER_PROFILE = 'SET_MOTHER_NAME_MOTHER_PROFILE';
export const SET_BABY_INFO_FROM_FIFTH_LOGIN = 'SET_BABY_INFO_FROM_FIFTH_LOGIN';
export const SET_USER_INFO_FROM_PROFILE_PAGE = 'SET_USER_INFO_FROM_PROFILE_PAGE';
export const SET_EMAIL_INPUT_BABY_PROFILE = 'SET_EMAIL_INPUT_BABY_PROFILE';
export const SET_AVATAR_SOURCE = 'SET_AVATAR_SOURCE';
export const SET_INCREMENT_WEEK_NUMBER = 'SET_INCREMENT_WEEK_NUMBER'
export const SET_CHILD_BIRTH_DAYS = 'SET_CHILD_BIRTH_DAYS'

export function setDaysTillChildbirthThirdLogin(childbirthDays, childbirthDateFormatted,weekNumber) {
  return {
    type: SET_DATE_FOR_CHILDBIRTH_LOGIN_TIRTH,
    childbirthDays,
    childbirthDateFormatted,
    weekNumber,
  }
}

export function setUserInfoFromFourthLogin(
                                            motherDay, 
                                            motherMonth, 
                                            motherYear, 
                                            avatarSource, 
                                            motherEmail, 
                                            motherName, 
                                            motherHeight, 
                                            motherWeight) {
  return {
    type: SET_USER_INFO_LOGIN_FORTH,
    motherDay,
    motherMonth,
    motherYear,
    avatarSource,
    motherEmail,
    motherName,
    motherHeight,
    motherWeight,
  }
}

export function setUserInfoFromProfilePage(
                                           childbirthDays,
                                           childbirthDateFormatted,
                                           motherDay,
                                           motherMonth,
                                           motherYear,
                                           avatarSource,
                                           motherName,
                                           motherHeight,
                                           motherWeight,
                                           weekNumber,
                                           selectedStartDate
                                           ) {
  return {
    type: SET_USER_INFO_FROM_PROFILE_PAGE,
    childbirthDays,
    childbirthDateFormatted,
    motherDay,
    motherMonth,
    motherYear,
    avatarSource,
    motherName,
    motherHeight,
    motherWeight,
    weekNumber,
    selectedStartDate
    
  }
}

export function setMotherNameMotherProfile(motherName) {
  return {
    type: SET_NAME_INPUT_MOTHER_PROFILE,
    motherName
  }
}
export function setMotherEmailBabyProfile(motherEmail) {
  return {
    type: SET_EMAIL_INPUT_BABY_PROFILE,
    motherEmail
  }
}

export function setBabyInfoFromFifthLogin(babyName, babySex, babyNumber) {
  return {
    type: SET_BABY_INFO_FROM_FIFTH_LOGIN,
    babyName,
    babySex,
    babyNumber,
  }
}

export function setAvatarSource(avatarSource) {
  return {
    type: SET_AVATAR_SOURCE,
    avatarSource
  }
}


export function incrementWeekNumber(weekNumber) {
  return {
    type: SET_INCREMENT_WEEK_NUMBER,
    weekNumber
  }
}

export function incrementChildBirthDays(childbirthDays){
  return {
    type: SET_CHILD_BIRTH_DAYS,
    childbirthDays
  }
}