import React from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import { connect } from 'react-redux'

import HomeScreen from '../Screens/HomeScreen';
import AboutScreen from '../Screens/AboutScreen';
import NotificationScreen from '../Screens/NotificationScreen';
import MotherProfileScreen from '../Screens/MotherProfileScreen';
import BabyProfileScreen from '../Screens/BabyProfileScreen';
import SideMenu from '../Screens/DrawerScreen';
import InfoScreen from '../Screens/InfoScreen';
import LogInFirst from '../Screens/Log In/LogInFirst';
import LogInSecond from '../Screens/Log In/LogInSecond';
import LogInThird from '../Screens/Log In/LogInThird';
import LogInFourth from '../Screens/Log In/LogInFourth';
import LogInFifth from '../Screens/Log In/LogInFifth';

import { width, height, totalSize } from 'react-native-dimension';

//Disable the Yellow warning box.................................... 
console.disableYellowBox = true;

const StackRegistrationPage = StackNavigator({
    LogInFirst: {
      screen: LogInFirst,
    },
    LogInSecond: {
      screen: LogInSecond,
    },
    LogInThird: {
      screen: LogInThird,
    },
    LogInFourth: {
      screen: LogInFourth,
    },
    LogInFifth: {
      screen: LogInFifth,
    },
    Home: {
      screen: HomeScreen
    },
    Info: {
      screen: InfoScreen,
    },
    MotherProfile: {
      screen: MotherProfileScreen,
    },
    BabyProfile: {
      screen: BabyProfileScreen,
    },
    About: {
      screen: AboutScreen,
    },
    Notification: {
      screen: NotificationScreen,
    },
  }
);


const StackHomeScreen = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  LogInFirst: {
    screen: LogInFirst,
  },
  LogInSecond: {
    screen: LogInSecond,
  },
  LogInThird: {
    screen: LogInThird,
  },
  LogInFourth: {
    screen: LogInFourth,
  },
  LogInFifth: {
    screen: LogInFifth,
  },
  Info: {
    screen: InfoScreen,
  },
  MotherProfile: {
    screen: MotherProfileScreen,
  },
  BabyProfile: {
    screen: BabyProfileScreen,
  },
  About: {
    screen: AboutScreen,
  },
  Notification: {
    screen: NotificationScreen,
  },
}
);


const getNavigationScreens = DrawerNavigator(
  {
    DrawerLogin: {
      screen: StackRegistrationPage,
    },
    DrawerHome: {
      screen: StackHomeScreen,
    },
    DrawerProfile: {
      screen: MotherProfileScreen,
    },
    DrawerNotification: {
      screen: NotificationScreen,
    },
    DrawerAboute: {
      screen: AboutScreen,
    },

  },
  {
    initialRouteName: 'DrawerLogin',
    drawerWidth: width(85),
    drawerPosition: 'right',
    contentComponent: SideMenu,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
  }
);

export default getNavigationScreens;