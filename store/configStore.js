import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk'
import reducer from '../reducer/reducers'
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';

const middleware = applyMiddleware(thunk);

const persistConfig = {
  key: 'user-info',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = createStore(persistedReducer, middleware);
export const persistor = persistStore(store);
