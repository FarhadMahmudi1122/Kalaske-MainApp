import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { Provider } from 'react-redux';
import { store, persistor } from './store/configStore'
import { PersistGate } from 'redux-persist/integration/react';


class mainApp extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App/>
        </PersistGate>
      </Provider>
    )
  }

}

AppRegistry.registerComponent('mainProject', () => mainApp);
