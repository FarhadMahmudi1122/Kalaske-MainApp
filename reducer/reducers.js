import { combineReducers } from 'redux'

import userInfo from './userInfo'
import registrationInfo from './registrationInfo'


export default combineReducers({
  userInfo,
  registrationInfo
})