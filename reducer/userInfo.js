import {
  ALERT_ACTION,
  SET_DATE_FOR_CHILDBIRTH_LOGIN_TIRTH,
  SET_USER_INFO_LOGIN_FORTH,
  SET_NAME_INPUT_MOTHER_PROFILE,
  SET_MOTHER_WEIGHT_MOTHER_PROFILE,
  SET_AVATAR_SOURCE_MOTHER_PROFILE,
  SET_DAY_MONTH_YEAR_MOTHER_PROFILE,
  SET_DATE_DAYS_MOTHER_PROFILE,
  SET_MOTHER_NAME_MOTHER_PROFILE,
  SET_MOTHER_HEIGHT_MOTHER_PROFILE,
  SET_BABY_INFO_FROM_FIFTH_LOGIN,
  SET_USER_INFO_FROM_PROFILE_PAGE,
  SET_EMAIL_INPUT_BABY_PROFILE,
  SET_AVATAR_SOURCE,
  SET_INCREMENT_WEEK_NUMBER,
  SET_CHILD_BIRTH_DAYS
} from '../actions/userActions';


const InitialState = {
  selectedStartDate:null,
  weekNumber:0,
  childbirthDays: null,
  childbirthDateFormatted: null,
  motherDay: 1,
  motherMonth: 'بهمن',
  motherYear: 1368,
  avatarSource: null,
  motherEmail: '',
  motherName: '',
  motherHeight: 160,
  motherWeight: 65,
  babyNumber: 1,
  babyName: '',
  babySex: '',
};

export default function reducer(state = InitialState, action) {
  switch (action.type) {
    case SET_DATE_FOR_CHILDBIRTH_LOGIN_TIRTH: {
      return {
        ...state,
        ...action
      };
    }
    case SET_USER_INFO_LOGIN_FORTH: {
      return {
        ...state,
        ...action
      };
    }
    case SET_MOTHER_NAME_MOTHER_PROFILE: {
      return {
        ...state,
        ...action,
      };
    }
    case SET_MOTHER_WEIGHT_MOTHER_PROFILE: {
      return {
        ...state,
        ...action,
      };
    }
    case SET_MOTHER_HEIGHT_MOTHER_PROFILE: {
      return {
        ...state,
        ...action,
      };
    }
    case SET_AVATAR_SOURCE_MOTHER_PROFILE: {
      return {
        ...state,
        ...action,
      };
    }
    case SET_DAY_MONTH_YEAR_MOTHER_PROFILE: {
      return {
        ...state,
        ...action
      };
    }
    case SET_DATE_DAYS_MOTHER_PROFILE: {
      return {
        ...state,
        ...action,
      };
    }
    case SET_BABY_INFO_FROM_FIFTH_LOGIN: {
      return {
        ...state,
        ...action
      }
    }
    case SET_USER_INFO_FROM_PROFILE_PAGE: {
      return {
        ...state,
        ...action
      }
    }
    case SET_EMAIL_INPUT_BABY_PROFILE : {
      return {
        ...state,
        ...action
      }
    }
    case SET_AVATAR_SOURCE :{
      return {
        ...state,
        ...action
      }
    }
    case SET_INCREMENT_WEEK_NUMBER :{
      return {
        ...state,
        ...action
      }
    }
    case SET_CHILD_BIRTH_DAYS :{
      return {
        ...state,
        ...action
      }
    }
    default:
      return state;
  }

}