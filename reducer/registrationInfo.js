import {
  SET_PHONE_NUMBER,
  REGISTRATION_COMPLETE,
  REGISTRATION_NOTIFICATION,
  REGISTRATION_NOT_COMPLETE
} from '../actions/registrationActions';

const InitialState = {
  phoneNumber: '',
  isRegistered: false,
  registrationNotification:false
};

export default function reducer(state = InitialState, action) {
  switch (action.type) {
    case SET_PHONE_NUMBER: {
      return { ...state, phoneNumber: action.phoneNumber };
    }
    case REGISTRATION_COMPLETE: {
      return { ...state, isRegistered: true };
    }
    case REGISTRATION_NOT_COMPLETE: {
      return { ...state, isRegistered: false };
    }
    case REGISTRATION_NOTIFICATION: {
      return { ...state, registrationNotification: true };
    }
    default:
      return state;
  }
}