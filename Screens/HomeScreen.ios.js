import React from 'react';
import {
    Text, 
    View, 
    StyleSheet, 
    ScrollView, 
    Image, 
    Platform, 
    TouchableOpacity,
    Animated,

} from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Card, Button } from 'react-native-elements';
import { width, height, totalSize } from 'react-native-dimension';
import Slider from 'react-native-slider';
import JalaliCalendarPicker from 'react-native-jalali-calendar-picker';
import Modal from "react-native-modal";


export default class HomeScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            visibleModal:null,
            constvalue:15,
            changeablevalue:-15,
            graySliderConterZindex:0,
            graySliderConterDisplay:'flex',
            changingSliderContentViewOpacity:new Animated.Value(0),
            constSliderContentViewOpacity:new Animated.Value(0),
            scrollY: new Animated.Value(0),
        };
        this.onDateChange = this.onDateChange.bind(this);
        
    }

    componentDidMount() {
        this.props.checkIsRegistered()
        this.props.navigation.setParams({
            that: this
        })
    }

    static navigationOptions = ({ navigation, screenProps }) => {
        return {
        title:  'بارداری هفته به هفته',
        headerStyle: {height: 35},
        headerTitleStyle:{
            color:'#000',
            fontWeight:'bold',
            fontFamily:'IRANSansMobile',
            fontSize:14,
            textAlign: 'center',
            alignSelf:'center',
            left:30
            
        },
        headerRight:
            <TouchableOpacity onPress={ () => { navigation.navigate('DrawerOpen') }}>
                <Image
                    source={require('../Assets/Images/BabySize/logo.png')}
                    style={[styles.drawerIcon]} />
            </TouchableOpacity>,
    }
};
    
    _handelSlidingStart = ()=>{
        Animated.timing(
            this.state.changingSliderContentViewOpacity,
            {
              toValue: 1,
              duration: 200,
              useNativeDriver: true
            }
          ).start();   
    }
    _handelSlidingComplete = ()=>{
        Animated.timing(
            this.state.changingSliderContentViewOpacity,
            {
              toValue: 0,
              duration: 3000,
              useNativeDriver: true
            }
          ).start();   
    }
    _handelOnPressSliderConter = ()=>{
        Animated.sequence([
            Animated.timing(
                this.state.constSliderContentViewOpacity,
                {
                  toValue: 1,
                  duration: 200,
                  useNativeDriver: true
                }),
            Animated.timing(
                this.state.constSliderContentViewOpacity,
                {
                  toValue: 0,
                  duration: 3000,
                  useNativeDriver: true
                })
        ]).start(); 
    }
    onDateChange(date) {
        this.setState({
          selectedStartDate: date,
        });
    }

    render(){
        console.log('height is',hright(100))
        const imageHeaderHeight = this.state.scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[height(20),0],
            extrapolate: 'clamp',
            useNativeDriver: true

          });
        const imageHeaderOpacity = this.state.scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[1,0],
            extrapolate: 'clamp',
            useNativeDriver: true
          });
        const SliderHeight = this.state.scrollY.interpolate({
            inputRange: [0,110],
            outputRange:[height(23),0],
            extrapolate: 'clamp',
            useNativeDriver: true

           });
        const SliderOpacity = this.state.scrollY.interpolate({
            inputRange: [0,110],
            outputRange:[1,0],
            extrapolate: 'clamp',
            useNativeDriver: true
            });
        const hiddenHeaderHeight = this.state.scrollY.interpolate({
            inputRange: [210,212],
            outputRange:[0,height(5)],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const hiddenHeaderOpacity = this.state.scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,0.5,1],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const HiddenSliderConterHeight = this.state.scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,25,50],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const HiddenSliderConterOpacity = this.state.scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,0.5,1],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const { selectedStartDate } = this.state;
        const startDate = selectedStartDate ? selectedStartDate.format('jYYYY/jM/jD') : '';
        return (
            <View style={[styles.container]}>
                <Modal
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={this.state.visibleModal === 5}
                    style={styles.bottomModal}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                    <Text>Bottom half modal</Text>
                    <TouchableOpacity onPress={() => this.setState({ visibleModal: null })}>
                        <View style={styles.button}>
                        <Text>Bottom half modal</Text>
                        </View>
                    </TouchableOpacity> 
                    </View>
                </Modal>
                <Modal
                    style={styles.calendarModal}
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={this.state.visibleModal === 2}
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={800}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={1200}
                    backdropTransitionOutTiming={800}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                        <JalaliCalendarPicker
                            onDateChange={this.onDateChange}
                            width={400}
                            height={400}
                        />
                        <Text>تاریخ انتخابی شما:{ startDate }</Text>
                        <TouchableOpacity onPress={() => this.setState({ visibleModal: null })}>
                            <View style={styles.button}>
                                <Text>تائید</Text>
                            </View>
                        </TouchableOpacity> 
                    </View>
                </Modal>

                <Animated.View style={[styles.hiddenHeader,{height:hiddenHeaderHeight,opacity:hiddenHeaderOpacity}]}>
                    <Text style={[styles.hiddenHeaderText]}>189روز تا زایمان</Text>
                </Animated.View>
                <Animated.View style={[styles.HiddenSliderConterView,{height:HiddenSliderConterHeight,opacity:HiddenSliderConterOpacity}]}>
                        <View
                            style={[styles.hiddenSliderConter]}
                        >
                            <Text style={[styles.hiddenSliderCounterText]}>{-this.state.changeablevalue}</Text>
                            <MaterialIcons
                                style={{position:'absolute',top:46,color:'#f4b642',alignSelf:'center'}}
                                name={'arrow-drop-down'}
                                size={4}
                            />
                        </View>
                </Animated.View>
                <ScrollView 
                    style={[styles.ScrollView]}
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}

                >
                    <View style={[styles.ScrollViewContent]}>
                        <TouchableOpacity 
                            onPress={() => this.props.navigation.navigate('Info')}
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image resizeMode="contain" style={[styles.listImage]} source={require('../Assets/Images/Main3.jpg')}/>
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader]}>ماساژ نوزاد</Text>
                                <Text style={[styles.listViewSubHeader]}>غذاهای چرب</Text>
                                <Text style={[styles.listViewDiscription]}>را کم کنید و سعی کنید و سرخ کرده را کم کنید و سعی کنید و سرخ کرده.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید و سرخ کرده</Text>
                            </View>
                            <View style={[styles.listViewColorSection,{backgroundColor:'#42f4e2'}]}></View>
                        </TouchableOpacity>

                        <View style={[styles.Footer]}>
                        </View>
                    </View>
                </ScrollView>
                <Animated.View style={[styles.Header]}>
                    <Animated.Image resizeMode="contain" style={[styles.headerImage,{height:imageHeaderHeight,opacity:imageHeaderOpacity}]} source={require('../Assets/Images/Test.jpg')}/>
                    {/* <Image resizeMode="contain" style={[styles.headerImage]} source={mainContent.HeaderImageUrls[i]}/> */}
                </Animated.View>
                <Animated.View style={[styles.sliderContiner,{elevation:-100,height:SliderHeight,opacity:SliderOpacity}]}>
                    <View style={[styles.sliderConterView,{right:20+(8*(this.state.constvalue))}]}>
                        <TouchableOpacity 
                            onPress={this._handelOnPressSliderConter}
                            style={[styles.sliderConter,{elevation:9,backgroundColor:'#999',zIndex:this.state.graySliderConterZindex}]}
                            activeOpacity={0.8}
                        >
                            <Text style={[styles.sliderCounterText]}>{this.state.constvalue}</Text>
                            <MaterialIcons
                                style={{position:'absolute',top:46,color:'#999',alignSelf:'center'}}
                                name={'arrow-drop-down'}
                                size={4}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.sliderConterView,{right:20+(8*(-this.state.changeablevalue)),elevation:10}]}>
                        <View 
                            style={[styles.sliderConter,{backgroundColor:'#f4b642'}]}
                            activeOpacity={0}
                        >
                            <Text style={[styles.sliderCounterText]}>{-this.state.changeablevalue}</Text>
                            <MaterialIcons
                                style={{position:'absolute',top:46,color:'#f4b642',alignSelf:'center'}}
                                name={'arrow-drop-down'}
                                size={4}
                            />
                        </View>
                    </View>
                    <View style={styles.slider}>
                        <Slider
                            style={{height:40,width:width(80)}}
                            thumbStyle={{width: 11, height: 11,backgroundColor:'rgba(100,100,100,0)'}}
                            trackStyle={{opacity:1}}
                            thumbTintColor={'#09f'}
                            thumbTouchSize={{width: 50, height: 50}}
                            minimumTrackTintColor={'#999'}
                            maximumTrackTintColor={'#f4b642'}
                            minimumValue={-40}
                            maximumValue={0}
                            step={1}
                            value={this.state.changeablevalue}
                            onValueChange={changeablevalue => this.setState({ changeablevalue })}

                        />
                    </View>
                    <View style={styles.sliderHiden}>
                        <Slider
                            style={{height:40,width:width(90)}}
                            thumbStyle={{width: 10, height: 10,backgroundColor:'rgba(100,100,100,0)'}}
                            trackStyle={{opacity:0}}
                            thumbTintColor={'#09f'}
                            thumbTouchSize={{width: 50, height: 50}}
                            minimumTrackTintColor={'#999'}
                            maximumTrackTintColor={'#f4b642'}
                            minimumValue={-40}
                            maximumValue={0}
                            step={1}
                            value={this.state.changeablevalue}
                            onValueChange={changeablevalue => {
                                this.setState({ changeablevalue, });
                                if(this.state.constvalue+3<-this.state.changeablevalue||this.state.constvalue-3>-this.state.changeablevalue){
                                    this.setState({graySliderConterZindex:1})
                                } else if(this.state.constvalue+3>-this.state.changeablevalue && this.state.constvalue-3<-this.state.changeablevalue){
                                    this.setState({graySliderConterZindex:0})
                                }
                            }}
                            onSlidingStart={this._handelSlidingStart}
                            onSlidingComplete={this._handelSlidingComplete}
                        />
                    </View> 
                    <Animated.View style={[styles.sliderContentView,{right:0+(8*(this.state.constvalue)),opacity:this.state.constSliderContentViewOpacity}]}>
                        <View style={[styles.sliderContent,{backgroundColor:'#42d4f4'}]}>
                            <MaterialIcons
                                style={{position:'absolute',top:-4,color:'#42d4f4',alignSelf:'center'}}
                                name={'arrow-drop-up'}
                                size={4}
                            />
                            <Text style={[styles.sliderCountentText]}>شما در هفته پنجم بارداری هستید</Text>
                        </View>
                    </Animated.View>
                    <Animated.View style={[styles.sliderContentView,{right:0+(8*(-this.state.changeablevalue)),opacity:this.state.changingSliderContentViewOpacity}]}>
                        <View style={[styles.sliderContent,{backgroundColor:'#42d4f4'}]}>
                            <MaterialIcons
                                style={{position:'absolute',top:-4,color:'#42d4f4',alignSelf:'center'}}
                                name={'arrow-drop-up'}
                                size={4}
                            />
                            <Text style={[styles.sliderCountentText]}>شما در هفته پنجم بارداری هستید</Text>
                        </View>
                    </Animated.View >
                    <View style={[styles.subHeader]}>
                        <Text style={[styles.subHeaderText]}>
                        غذاهای چرب و سرخ کرده را کم کنید و سعی کنید در طول هفته هر روز حداقل نیم ساعت ورزش داشته باشی.غذاهای چرب و سرخ کرده را کم کنید و سعی کنید در طول هفته هر روز حداقل نیم ساعت ورزش داشته باشید.</Text>
                        <Image style={[styles.subHeaderImage]} source={require('../Assets/Images/BabySize/40.png')} />
                    </View>
                    <View style={[styles.bacgkroundSubHeaderView]}></View>
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    dateIcon:{
        paddingLeft:10,
        color:'#000',
        display:'none'
        

    },
    drawerIcon:{
        paddingRight:10,
        width:30,
        height:33,
        marginRight:15

    },
    container:{
        flex:1,
        backgroundColor:'white'
    },
    button: {
        backgroundColor: "lightblue",
        padding: 12,
        margin: 16,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    bottomModal: {
        justifyContent: "flex-end",
        marginTop: 300,
        marginLeft:0,
        marginRight:0,
        marginBottom:0
    },
    modalContent: {
        height:350,
        flex:1,
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    calendarModal:{
        marginTop: 100,
        marginLeft:10,
        marginRight:10,
        marginBottom:120
    },
    hiddenHeader:{
        width:width(100),
        position:'absolute',
        top:0,
        right:0,
        left:0,
        backgroundColor:'#ffefa6',
        zIndex:1
    },
    hiddenHeaderText:{
        alignSelf:'flex-start',        
        top:8,
        left:10,
        fontFamily:'IRANSansMobile',
        color:'#000',
        fontSize:15,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
    },
    HiddenSliderConterView:{
        elevation:10,
        position:'absolute',
        top:0,
        right:20,
        zIndex:10

    },
    hiddenSliderConter:{
        backgroundColor:'#f4b642',
        width:50,
        height:50,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderWidth:2,
        borderColor:'#fff',
        borderRadius:50,

    },
    hiddenSliderCounterText:{
        top:10,
        fontFamily:'IRANSansMobile',
        color:'#fff',
        fontSize:24,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
    },
    ScrollView:{
        flexGrow:1,
    },
    ScrollViewContent:{
        marginTop:300,
        flex: 1,
    },
    listTouchable:{
        flexDirection:'row',
        alignItems:'center',
        height:height(24),
        borderColor:'#ddd',
        borderBottomWidth:0.8
    },
    listViewContent:{
        width:width(55),
        marginRight:5,
        marginLeft:-10
    },
    listViewHeader:{
        fontFamily:'IRANSansMobile',
        color:'#000',
        fontSize:24,
        fontWeight:'bold',
        textAlign:'justify',
        writingDirection:'rtl'
    },
    listViewSubHeader:{
        fontFamily:'IRANSansMobile',
        color:'#999',
        fontSize:12,
        textAlign:'justify',
        writingDirection:'rtl'
    },
    listViewDiscription:{
        fontFamily:'IRANSansMobile',
        color:'#000',
        fontSize:12,
        textAlign:'justify',
        writingDirection:'rtl'
    },
    listViewColorSection:{
        width:width(10),
        height:height(22),

    },
    listImage:{
        width:width(43),
    },
    Footer:{
        height:20,
    },
    Header:{
        position:'absolute',
        top:0,
        right:0,
        left:0
    },
    headerImage:{
        width:width(60),
        height:height(20),
        resizeMode:'cover',
        alignSelf:'center',
    },
    sliderContiner:{
        position:'absolute',
        top:height(20),
        right:0,
        left:0,
        
    },
    sliderConterView:{
        position:'absolute',
        top:0,
        left:0
    },
    sliderConter:{
        width:50,
        height:50,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderWidth:2,
        borderColor:'#fff',
        borderRadius:50,

    },
    sliderCounterText:{
        top:10,
        fontFamily: 'IRANSansMobile',
        color:'#fff',
        fontSize:24,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
        
    },
    slider:{
        width:width(80),
        alignSelf:'center',
        top:35,
        position:'absolute'
    },
    sliderHiden:{
        alignSelf:'center',
        top:-10,
        position:'absolute'
    },
    sliderContentView:{
        position:'absolute',
        top:60,
        left:0
    },
    sliderContent:{
        alignContent:'center',        
        width:90,
        height:40,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderWidth:2,
        borderColor:'#fff',
        backgroundColor:'#999',
        borderBottomLeftRadius:17,
        borderTopLeftRadius:20,
        borderBottomRightRadius:17,
        borderTopRightRadius:20,
    },
    sliderCountentText:{
        top:4,
        left:10,
        fontFamily:'IRANSansMobile',
        color:'#fff',
        fontSize:9,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)',
        justifyContent:'flex-start',
        alignSelf:'flex-start',
        alignItems:'flex-start',
        textAlign:'right',
        writingDirection:'rtl'
    },
    subHeader:{
        alignSelf:'center',
        flexDirection:'row',
        top:60,
        zIndex:-100,
        backgroundColor:'rgba(1,1,1,0)'
    },
    subHeaderImage:{
        width:60,
        height:60,
    },
    subHeaderText:{
        fontFamily: 'IRANSansMobile',
        width:width(80),
        padding:10,
        marginTop:0,
        marginRight:10,
        textAlign:'justify',
        writingDirection:'rtl'

    },
    bacgkroundSubHeaderView:{
        position:'absolute',
        backgroundColor:'#eee',
        top:0,
        height:160,
        width:width(100),
        zIndex:-1000,
        borderTopWidth:0.8,
        borderColor:'#ddd',
        borderBottomWidth:0.8
    },


});