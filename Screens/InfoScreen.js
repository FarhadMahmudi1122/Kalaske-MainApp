import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
  Platform,
  WebView,
  Clipboard,
  ToastAndroid,
  AlertIOS,
  TouchableOpacity
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { width, height, totalSize } from 'react-native-dimension';
import Share, { ShareSheet, Button } from 'react-native-share';


export default class InfoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      scrollViewDisplay:'flex',
      headerDisplay:'flex',
      fullScreenModeButtonDisplay:'flex',
      normalScreenModeButtonBottom:-100
    }
    this.onShareOpen = this.onShareOpen.bind(this);
    this.onShareCancel = this.onShareCancel.bind(this);
  }

  static navigationOptions = ({ navigation, screenProps }) => {
    return {header:null}
  };

  shareOptions = {
    title: 'React Native',
    message: 'اپلیکیشن کالسکه را می توانید از سایت زیر دانلود کنید',
    url: 'http://kaaleskeh.ir/',
    subject: 'Share Link' // for email
  }

  onShareCancel() {
    //console.log("CANCEL")
    this.setState({ visible: false });
  }

  onShareOpen() {
    //console.log("OPEN")
    Share.open(this.shareOptions);
  }

  componentWillMount() {
    this.props.navigation.setParams({ 
      onShareOpen: this.onShareOpen,
      normalScreen: this.state.normalScreen 
    });
  }

  _handleFullScreenModeButton(){
    this.setState({
      scrollViewDisplay:'none',
      fullScreenModeButtonDisplay:'none',
      headerDisplay:'none',
      normalScreenModeButtonBottom:0
    })
  }

  _handleNormalScreenModeButton(){
    this.setState({
      scrollViewDisplay:'flex',
      fullScreenModeButtonDisplay:'flex',
      headerDisplay:'flex',
      normalScreenModeButtonBottom:-100
    }) 
  }


  //......................Handle Responsive Style......................


  _handleHeaderFontSize(){
    if(height(100)<600){
        return(16)
    }else{
        return(20)
    }
  }

  _handleDescriptionFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  render() {
    const { 
      scrollViewDisplay,
      fullScreenModeButtonDisplay,
      normalScreenModeButtonBottom,
      headerDisplay 
    } = this.state
    const { card } = this.props.navigation.state.params;
    const videoUrl = card.FullContent.ContentMedium.Url + '?sbv=false&ttv=false&rpv=false&sbv=false';
    const imageUrl = 'http://rafshadow.ir' + (card.FullContent.ContentMedium.Url);
    const fullContentText = card.FullContent.Text
    const contentLength = card.FullContent.Header.length + card.FullContent.Subheader.length + fullContentText.length;
    const colorSection = contentLength > 310 ? contentLength / 1.6 : contentLength / 1.1;
    if (card.FullContent.ContentMedium.Type === 'video/mp4') {
      return (
        <View style={[styles.container]}>
          <View style={[styles.headerIcons,{display:headerDisplay }]}>
            <View style={[styles.leftIcons]}>
              <MaterialIcons
                style={[styles.shareIcon]}
                name={'share'}
                size={20}
                onPress={this.onShareOpen}
              />
            </View>
            <View style={[styles.backIcons]}>
              <Text style={[styles.backText]} 
                onPress={() => {this.props.navigation.goBack()}}>
                بازگشت
              </Text>
              <MaterialIcons
                style={[styles.backIcon]}
                name={'keyboard-arrow-right'}
                size={30}
                onPress={() => {this.props.navigation.goBack()}}
                />
              </View>
          </View>
          <WebView
            style={[styles.video]}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            scalesPageToFit={true}
            allowsInlineMediaPlayback={true}
            automaticallyAdjustContentInsets={true}
            source={{ uri: videoUrl }}
          />
          <TouchableOpacity
            style={[styles.fullScreenModeBTN,{
              display:fullScreenModeButtonDisplay
            }]}
            onPress={()=>this._handleFullScreenModeButton()}
          />
          <ScrollView 
            style={[styles.scrollView,{ flexGrow: 1,display:scrollViewDisplay }]}
          >
            <View style={[styles.ScrollViewContent]}>
              <View style={[styles.descWrapper]}>
                <Text style={[styles.hedertextBox,{
                  fontSize: this._handleHeaderFontSize()
                }]}>
                  {card.FullContent.Header}
                </Text>
                <Text style={[styles.subHedertextBox]}>
                  {card.FullContent.Subheader}
                </Text>
                <Text style={[styles.textBox,{
                   fontSize:this._handleDescriptionFontSize()
                }]}>
                  {fullContentText}
                </Text>
              </View>
              <View style={[styles.listViewColorSection, {
                backgroundColor: card.SubjectCategory.Color,
                height: colorSection,

              }]}
              >
              </View>
            </View>
            <View style={[styles.Footer]}>
            </View>
          </ScrollView>
          <TouchableOpacity
            style={[styles.normalScreenModeBTN,{
              bottom:normalScreenModeButtonBottom,
            }]}
            onPress={()=>this._handleNormalScreenModeButton()}
          />
          <ShareSheet
            visible={this.state.visible}
            onShareCancel={this.onShareCancel.bind(this)}
          >
          </ShareSheet>
        </View>
      )
    } else {
      return (
        <View style={[styles.container]}>
          <View style={[styles.headerIcons]}>
            <View style={[styles.leftIcons]}>
              <MaterialIcons
                style={[styles.shareIcon]}
                name={'share'}
                size={20}
                onPress={this.onShareOpen}
              />
            </View>
            <View style={[styles.backIcons]}>
              <Text style={[styles.backText]} 
                onPress={() => {this.props.navigation.goBack()}}>
                بازگشت
              </Text>
              <MaterialIcons
                style={[styles.backIcon]}
                name={'keyboard-arrow-right'}
                size={30}
                onPress={() => {this.props.navigation.goBack()}}
                />
              </View>
          </View>
          <Image
            style={[styles.headerImage]}
            source={{ uri: imageUrl }}
          />
          <ScrollView
            style={{ flexGrow: 1 }}
            ref='scroll'
          >
            <View style={[styles.ScrollViewContent]}>
              <View style={[styles.descWrapper]}>
                <Text style={[styles.hedertextBox,{
                  fontSize: this._handleHeaderFontSize()          
                }]}>
                  {card.FullContent.Header}
                </Text>
                <Text style={[styles.subHedertextBox]}>
                  {card.FullContent.Subheader}
                </Text>
                <Text style={[styles.textBox,{
                  fontSize:this._handleDescriptionFontSize()
                }]}>
                  {fullContentText}
                </Text>
              </View>
              <View style={[styles.listViewColorSection, {
                backgroundColor: card.SubjectCategory.Color,
                height: colorSection,

              }]}
              >
              </View>
            </View>
            <View style={[styles.Footer]}>
            </View>
          </ScrollView>

          <ShareSheet
            visible={this.state.visible}
            onShareCancel={this.onShareCancel.bind(this)}
          >
          </ShareSheet>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerIcons:{
    height:35,
    backgroundColor: 'white',
    elevation: 5,
    flexDirection: 'row',
  },
  leftIcons: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    marginRight:20,
  },
  shareIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 30,
    marginRight:100,    
    color: '#000',
  },
  backIcons: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  backText: {
    marginRight: -9,
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
    marginLeft:120,    
  },
  backIcon: {
    color: '#000',
  },
  video:{
    flex:1
  },
  fullScreenModeBTN:{
    width:25,
    height:25,
    backgroundColor:'rgba(0,0,0,0)',
    alignSelf:'flex-end',
    top:-30,
    zIndex:10,
    right:13
  },
  scrollView:{

  },
  headerImage: {
    width: width(100),
    height: height(30)
  },
  ScrollViewContent: {
    flex: 1,
    padding: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  hedertextBox: {
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    color: '#000',
    fontWeight: 'bold',
    textAlign: 'right',

  },
  subHedertextBox: {
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    color: '#000',
    fontSize: 10,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  textBox: {
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    color: '#000',
    marginBottom: 10,
    marginTop: 10,
    lineHeight: 30,
    textAlign: 'right',
  },
  listViewColorSection: {
    width: width(5),
    right: -15,
    top: 0
  },
  Footer: {
    height: 20,
  },
  normalScreenModeBTN:{
    width:25,
    height:25,
    backgroundColor:'rgba(0,0,0,0)',
    alignSelf:'flex-end',
    zIndex:10,
    position:'absolute',
    right:13,
  }
});







