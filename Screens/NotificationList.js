import React from 'react';
import {
    Text,
    View,
    StyleSheet, 
    ScrollView, 
    Image, 
    Platform,
    TouchableOpacity
} from "react-native";
import { width, height, totalSize } from 'react-native-dimension';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'


export default class NotificationList extends React.Component {
    state = {
        notifDisplay: 'none',

    };

    componentDidMount(){
        this._handleNotificationListDisplay()
    }

    _handleNotificationListDisplay(){
        if(this.props.registrationNotification){
            this.setState({
                notifDisplay: 'flex'
            })
        }
    }

    render(){
        const {notifDisplay} = this.state
        const {registrationNotification} = this.props
        const registrationNotificationText = registrationNotification ? 'عضویت شما در سرویس با موفقیت ثبت شد.':null
        return (
            <View style={[styles.container,{display:notifDisplay}]}>
                <Text style={styles.contentText}>{registrationNotificationText}</Text>
                <TouchableOpacity  
                    style={styles.clearButton}
                    onPress={()=>{
                        this.setState({
                            notifDisplay: 'none'
                        })
                    }}
                >
                    <MaterialIcons
                        style={[styles.clearIcon]}
                        name={'clear'}
                        size={30}
                    />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop:5,
        flexDirection:'row',
        backgroundColor:'#42d4f4',
        justifyContent:'flex-end',
        alignItems: 'center'
    },
    contentText:{
        flex: 1,        
        textAlign:'center',
        backgroundColor:'#42d4f4',
        justifyContent:'center',
        alignItems: 'center'
    },
    clearButton:{
        
    },
    clearIcon:{
        color:'#fff',
    }

});