import React from 'react';
import {
    Text, 
    View, 
    StyleSheet,
    ScrollView,
    Image,
    Platform,
    TouchableOpacity,
    Animated,
    ActivityIndicator,
    ToastAndroid
} from "react-native";
import { bindActionCreators } from 'redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Card, Button } from 'react-native-elements';
import { width, height, totalSize } from 'react-native-dimension';
import Slider from 'react-native-slider';
import JalaliCalendarPicker from 'react-native-jalali-calendar-picker';
import Modal from "react-native-modal";
import { mainPage , weekNumberForConstValue} from "../JsonData/Data";
import { connect } from 'react-redux'
import RenderCards from './Cards';
import * as UserActions from '../actions/userActions';
import * as RegistrationActions from '../actions/registrationActions';
import moment from 'moment-jalaali';
import DefaultSliderData from './DefaultData/DefaultSliderData';
import DefaultHeaderImageData from './DefaultData/DefaultHeaderImageData';

function mapStateToProps({userInfo, registrationInfo}){
    return {
        childbirthDateFormatted:userInfo.childbirthDateFormatted,
        weekNumber: userInfo.weekNumber,
        isRegistered: registrationInfo.isRegistered,
        motherDay: userInfo.motherDay,
        motherMonth: userInfo.motherMonth,
        motherYear: userInfo.motherYear,
        motherEmail: userInfo.motherEmail,
        motherName: userInfo.motherName,
        motherHeight: userInfo.motherHeight,
        motherWeight: userInfo.motherWeight,
        babyNumber: userInfo.BabyNumber,
        babyName: userInfo.babyName,
        babySex: userInfo.babySex,
        phoneNumber: registrationInfo.phoneNumber
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        ...UserActions,
        ...RegistrationActions
    }, dispatch)
}
  
@connect(mapStateToProps, mapDispatchToProps)
export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultData:false,
            noConnectionError:false,
            indicatorDisplay:true,
            headerImage:null,
            sliderImage:null,
            sliderText:null,
            cards:[],
            visibleModal:null,
            constvalue:this.props.weekNumber,
            changeablevalue:-this.props.weekNumber,
            graySliderConterZindex:0,
            graySliderConterDisplay:'flex',
            changingSliderContentViewOpacity:new Animated.Value(0),
            constSliderContentViewOpacity:new Animated.Value(0),
            scrollY: new Animated.Value(0),
        };
        this.onDateChange = this.onDateChange.bind(this);
    }

    componentDidMount(){
        this.props.checkIsRegistered(this.props.isRegistered , this.props.phoneNumber)
        this.fetchWeek(-this.state.changeablevalue);
        this.sendProfileInfo(); 
        this.toastAndroid();
        this.incrementalWeekNumber();
        this.setDefaultData();
    }

    // Dispatch Functions
    incrementWeekNumber(weekNumber){
        this.props.incrementWeekNumber(weekNumber)
    }
    


    setDefaultData(){
        const {noConnectionError} = this.state;
        if(noConnectionError){
            setTimeout(()=>{
                this.setState({
                    defaultData:true
                })
            },2000)
        }
    }

    incrementalWeekNumber(){
        const { childbirthDateFormatted } = this.props
        if(childbirthDateFormatted){
            const gregorianDate = moment(childbirthDateFormatted, 'jYYYY/jM/jD').format('YYYY/M/D');
            const selectedStartDateForBirth = moment(gregorianDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
            const weekNumber = 40-(Math.ceil(Math.ceil(
                (moment(selectedStartDateForBirth).valueOf()-moment().valueOf()) /
                (1000 * 60 * 60 * 24))/7)-1)
            this.incrementWeekNumber(weekNumber);
            console.log('newWeekNumber is ............... ',weekNumber) 
        }
    }

    toastAndroid(){
        if (this.props.isRegistered === false) {
            ToastAndroid.show('شما هنوز ثبت نام نکرده اید', ToastAndroid.SHORT);
        } 
    }

    sendProfileInfo(){
        console.log('profile sent ')
        const {
            weekNumber,
            motherDay,
            motherMonth,
            motherYear,
            motherEmail,
            motherName,
            motherHeight,
            motherWeight,
            babyNumber,
            babyName,
            babySex,
        } = this.props
        let motherBirthday = `${motherDay}${motherMonth}${motherYear}`
        let json = {
            weekNumber:weekNumber,
            motherEmail:motherEmail,
            motherName:motherName,
            motherHeight:motherHeight,
            motherWeight:motherWeight,
            babyNumber:babyNumber,
            babyName:babyName,
            babySex:babySex,
            motherBirthday:motherBirthday,
        }
        fetch(`http://rafshadow.ir/profile`,{
            method:'POST',
            headers: {
                'content-type' : 'application/json'
            },
            body: JSON.stringify(json)
        })
        .then((response)=> response.json())
        .then((res)=>{
            console.log('send profile response :',res)
        })
    }

    fetchWeek(weekNumber) {
        if(this.props.isRegistered && weekNumber === this.props.weekNumber){
            var day = (new Date().getDay() +1) %7
            console.log("new Date " + day);                        
            console.log("fetching week " + weekNumber);            
            fetch(`http://rafshadow.ir/content-list/kaaleskeh/${weekNumber}/${day}`)
            .then((response) => response.json())
            .then((res) => {
                if(res){
                    this.setState({
                        cards:res.day.Cards,
                        headerImage:res.headerImage,
                        sliderImage:res.sliderImage,
                        sliderText:res.sliderText,
                        indicatorDisplay:false,
                        noConnectionError:false
                    })
                    console.log('send weeknumber and day response: ', res);
                }
            })
            .catch((error)=>{
                console.log(error)
                this.setState({
                    noConnectionError:true
                })
            })
        
        }else if(this.props.isRegistered && weekNumber < this.props.weekNumber &&
                (this.props.weekNumber-3) < weekNumber || 
                weekNumber === 0){
            console.log("fetching week " + weekNumber);            
            fetch(`http://rafshadow.ir/content-list/kaaleskeh/${weekNumber}/6`)
            .then((response) => response.json())
            .then((res) => {
                if(res){
                    this.setState({
                        cards:res.day.Cards,
                        headerImage:res.headerImage,
                        sliderImage:res.sliderImage,
                        sliderText:res.sliderText,
                        indicatorDisplay:false,
                    })
                    console.log('send weeknumber and day response: ', res);
                }
            })
            .catch((error)=>{
                console.log(error)
                this.setState({
                    noConnectionError:true
                })
            })
        }else {
            var day = (new Date().getDay() +1) %7
            fetch(`http://rafshadow.ir/content-list/kaaleskeh/${weekNumber}/${day}`)
            .then((response) => response.json())
            .then((res) => {
                if(res){
                    this.setState({
                        headerImage:res.headerImage,
                        sliderImage:res.sliderImage,
                        sliderText:res.sliderText,
                    })
                    console.log('send weeknumber and day response: ', res);
                }
            })
        }

    }

    static navigationOptions = ({ navigation, screenProps }) => {
        return {
        title:  'بارداری هفته به هفته',
        headerStyle: {height: 35},
        headerTitleStyle:{
            color:'#000',
            fontWeight:'bold',
            fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
            fontSize:14,
            textAlign: 'center',
            alignSelf:'center',
            left:30,
        },
        headerLeft: null,
        headerRight:
            <TouchableOpacity 
                onPress={ () => { navigation.navigate('DrawerOpen') }}
            >
                <Image
                    source={require('../Assets/Images/logo.png')}
                    style={[styles.drawerIcon]}
                />
            </TouchableOpacity>,
    }
};
    
    _handelSlidingStart = ()=>{
        Animated.timing(
            this.state.changingSliderContentViewOpacity,
            {
              toValue: 1,
              duration: 200,
              useNativeDriver: true
            }
          ).start();   
    }
    _handelSlidingComplete = ()=>{
        Animated.timing(
            this.state.changingSliderContentViewOpacity,
            {
              toValue: 0,
              duration: 3000,
              useNativeDriver: true
            }
          ).start();   
    }
    _handelOnPressSliderConter = ()=>{
        Animated.sequence([
            Animated.timing(
                this.state.constSliderContentViewOpacity,
                {
                  toValue: 1,
                  duration: 200,
                  useNativeDriver: true
                }),
            Animated.timing(
                this.state.constSliderContentViewOpacity,
                {
                  toValue: 0,
                  duration: 3000,
                  useNativeDriver: true
                })
        ]).start(); 
    }
    onDateChange(date) {
        this.setState({
          selectedStartDate: date,
        });
    }

    //......................Handle Responsive Style......................

    _handleSliderHeight(){
        if(height(100)<600){
            return(height(30))
        }else{
            return(height(23))
        }
    }

    _handleScrollViewContentMarginTop(){
        if(height(100)<600){
            return(height(48))
        }else{
            return(height(42))
        }
    }


    render(){
        
        const {
            defaultData,
            noConnectionError,
            indicatorDisplay,
            headerImage,
            sliderImage,
            sliderText,
            cards,
            visibleModal,
            constvalue,
            changeablevalue,
            graySliderConterZindex,
            graySliderConterDisplay,
            selectedStartDate,
            changingSliderContentViewOpacity,
            constSliderContentViewOpacity,
            scrollY,
        } = this.state
        const imageHeaderHeight = scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[height(20),0],
            extrapolate: 'clamp',
            useNativeDriver: true

        });
        const imageHeaderOpacity = scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[1,0],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const SliderHeight =scrollY.interpolate({
            inputRange: [0,110],
            outputRange:[this._handleSliderHeight(),0],
            extrapolate: 'clamp',
            useNativeDriver: true

        });
        const SliderOpacity = scrollY.interpolate({
            inputRange: [0,110],
            outputRange:[1,0],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const hiddenHeaderHeight = scrollY.interpolate({
            inputRange: [210,212],
            outputRange:[0,height(5)],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const hiddenHeaderOpacity = scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,0.5,1],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const HiddenSliderConterHeight = scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,25,50],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        const HiddenSliderConterOpacity = scrollY.interpolate({
            inputRange: [210,212,220],
            outputRange:[0,0.5,1],
            extrapolate: 'clamp',
            useNativeDriver: true
        });
        
        const dayToBirth = (40-constvalue)*7;        
        const startDate = selectedStartDate ? selectedStartDate.format('jYYYY/jM/jD') : '';
        const sliderButtonZindexInRange = (constvalue+3) < -changeablevalue || (constvalue-3) > -changeablevalue;
        const sliderButtonZindexOutRange = (constvalue+3) > -changeablevalue && (constvalue-3) < -changeablevalue;
        const sliderConterViewConstvalue = (width(10)-20)+((width(78)/40)*constvalue);
        const sliderConterViewChangeablevalue = (width(10)-20)+((width(78)/40)*(-changeablevalue));
        const sliderContentViewConstvalue = (-2+((width(78)/40)*(constvalue)));
        const sliderContentViewChangeablevalue = (-2+((width(78)/40)*(-changeablevalue)))
        const headerImageUrl = 'http://rafshadow.ir'+(headerImage);
        const sliderImageUrl = 'http://rafshadow.ir'+(sliderImage);

        
        return (
            <View style={[styles.container]}>

                <ActivityIndicator 
                    size="large" 
                    color="#42d4f4" 
                    animating={indicatorDisplay}
                    style={[styles.indicator]}
                />
                <Modal
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={visibleModal === 5}
                    style={styles.bottomModal}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                    <Text>Bottom half modal</Text>
                    <TouchableOpacity 
                        onPress={() => this.setState({ visibleModal: null })}
                    >
                        <View style={styles.button}>
                        <Text>Bottom half modal</Text>
                        </View>
                    </TouchableOpacity> 
                    </View>
                </Modal>
                <Modal
                    style={styles.calendarModal}
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={visibleModal === 2}
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={800}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={1200}
                    backdropTransitionOutTiming={800}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                        <JalaliCalendarPicker
                            onDateChange={this.onDateChange}
                            width={400}
                            height={400}
                        />
                        <Text>تاریخ انتخابی شما:{ startDate }</Text>
                        <TouchableOpacity 
                            onPress={() => this.setState({ visibleModal: null })}
                        >
                            <View style={styles.button}>
                                <Text>تائید</Text>
                            </View>
                        </TouchableOpacity> 
                    </View>
                </Modal>

                <Animated.View 
                    style={[styles.hiddenHeader,{
                        height:hiddenHeaderHeight,
                        opacity:hiddenHeaderOpacity
                    }]}
                >
                    <Text 
                        style={[styles.hiddenHeaderText]}
                    >
                        {dayToBirth} روز تا زایمان
                    </Text>
                </Animated.View>
                <Animated.View 
                    style={[styles.HiddenSliderConterView,{
                        zIndex:10,
                        height:HiddenSliderConterHeight,
                        opacity:HiddenSliderConterOpacity
                    }]}
                >
                    <TouchableOpacity 
                        onPress={() => { this.refs._scrollView.scrollTo({y:0,animated:true})}}
                        style={[styles.hiddenSliderConter]}
                    >
                        <Text 
                            style={[styles.hiddenSliderCounterText]}
                        >
                            {-changeablevalue}
                        </Text>
                        <MaterialIcons
                            style={[styles.hiddenSliderConterIcon]}
                            name={'arrow-drop-down'}
                            size={19}
                        />
                    </TouchableOpacity>
                </Animated.View>
                <ScrollView 
                    ref='_scrollView'
                    style={[styles.ScrollView]}
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: scrollY}}}]
                    )}

                >

{/*////////////////////////////////////////////////////////////// Render Card*/}

                    <View 
                        style={[styles.ScrollViewContent,{
                            marginTop:this._handleScrollViewContentMarginTop()
                        }]}
                    >
                        <RenderCards 
                            noConnectionError = {noConnectionError}
                            changeablevalue={-changeablevalue} 
                            cards={cards} 
                            navigation={this.props.navigation}
                            defaultData={defaultData}
                        />
                        <View style={[styles.Footer]}></View>
                    </View>
                </ScrollView>

{/*////////////////////////////////////////////////////////////// Render HeaderImage*/}

                <DefaultHeaderImageData
                    scrollY = {scrollY}
                    headerImageUrl = {headerImageUrl}
                    noConnectionError = {noConnectionError}
                />
                <Animated.View 
                    style={[styles.sliderContiner,{
                        elevation:-100,
                        height:SliderHeight,
                        opacity:SliderOpacity
                    }]}
                >
                    <View 
                        style={[styles.sliderConterView,{
                            right:sliderConterViewConstvalue
                        }]}
                    >
                        <TouchableOpacity 
                            onPress={this._handelOnPressSliderConter}
                            style={[styles.sliderConter,{
                                elevation:9,
                                backgroundColor:'#999',
                                zIndex:graySliderConterZindex
                                }]}
                            activeOpacity={0.8}
                        >
                            <Text 
                                style={[styles.sliderCounterText]}
                            >
                                {constvalue}
                            </Text>
                            <MaterialIcons
                                style={[styles.sliderCounterTextIcon,{
                                    color:'#999',
                                }]}
                                name={'arrow-drop-down'}
                                size={19}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.sliderConterView,{
                        right:sliderConterViewChangeablevalue,
                        elevation:10}]}
                    >
                        <View 
                            style={[styles.sliderConter,{
                                backgroundColor:'#f4b642',
                            }]}
                            activeOpacity={0}
                        >
                            <Text style={[styles.sliderCounterText]}>
                                {-changeablevalue}
                            </Text>
                            <MaterialIcons
                                style={[styles.sliderCounterTextIcon]}
                                name={'arrow-drop-down'}
                                size={19}
                            />
                        </View>
                    </View>
                    <View style={styles.slider}>
                        <Slider
                            style={{height:25,width:width(80)}}
                            thumbStyle={{
                                width: 11, 
                                height: 11,
                                backgroundColor:'rgba(100,100,100,0)'
                            }}
                            trackStyle={{opacity:1}}
                            thumbTintColor={'#09f'}
                            thumbTouchSize={{width: 50, height: 50}}
                            minimumTrackTintColor={'#999'}
                            maximumTrackTintColor={'#f4b642'}
                            minimumValue={-40}
                            maximumValue={0}
                            step={1}
                            value={changeablevalue}
                            disabled={true}
                            onValueChange={changeablevalue => {
                                this.setState({ changeablevalue });
                            }}
                        />
                    </View>
                    <View style={styles.sliderHiden}>
                        <Slider
                            style={{height:50,width:width(90)}}
                            thumbStyle={{
                                width: 10, 
                                height: 10,
                                backgroundColor:'rgba(100,100,100,0)'
                            }}
                            trackStyle={{opacity:0}}
                            thumbTintColor={'#09f'}
                            thumbTouchSize={{width: 50, height: 50}}
                            minimumTrackTintColor={'#999'}
                            maximumTrackTintColor={'#f4b642'}
                            minimumValue={-40}
                            maximumValue={0}
                            step={1}
                            value={changeablevalue}
                            onValueChange={changeablevalue => {
                                this.setState({ changeablevalue, });
                                if(sliderButtonZindexInRange){
                                    this.setState({graySliderConterZindex:1})
                                } else if(sliderButtonZindexOutRange){
                                    this.setState({graySliderConterZindex:0})
                                }
                            }}
                            onSlidingStart={this._handelSlidingStart}
                            onSlidingComplete={()=>{
                                this._handelSlidingComplete();
                                this.fetchWeek(-changeablevalue);
                                }}
                        />
                    </View> 
                    <Animated.View 
                        style={[styles.sliderContentView,{
                            right:sliderContentViewConstvalue,
                            opacity:constSliderContentViewOpacity
                        }]}
                    >
                        <View 
                            style={[styles.sliderContent,{
                                backgroundColor:'#42d4f4'
                                }]}
                            >
                            <MaterialIcons
                                style={[styles.sliderContentIcons]}
                                name={'arrow-drop-up'}
                                size={19}
                            />
                            <Text 
                                style={[styles.sliderCountentText]}
                            >
                                شما در هفته {weekNumberForConstValue[constvalue]} بارداری هستید
                            </Text>
                        </View>
                    </Animated.View>
                    <Animated.View 
                        style={[styles.sliderContentView,{
                            right:sliderContentViewChangeablevalue,
                            opacity:changingSliderContentViewOpacity,
                        }]}
                     >
                        <View style={[styles.sliderContent,{
                            backgroundColor:'#42d4f4'
                            }]}
                        >
                            <MaterialIcons
                                style={[styles.sliderContentIcons]}
                                name={'arrow-drop-up'}
                                size={19}
                            />
                            <Text 
                                style={[styles.sliderCountentText]}
                            >
                                شما در هفته {weekNumberForConstValue[-changeablevalue]} بارداری هستید
                            </Text>
                        </View>
                    </Animated.View>

{/*////////////////////////////////////////////////////////////// Render SliderText*/}

                    <DefaultSliderData 
                        sliderText = {sliderText} 
                        sliderImageUrl = {sliderImageUrl}
                        noConnectionError = {noConnectionError}
                    />
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    dateIcon:{
        paddingLeft:10,
        color:'#000',
        display:'none'

    },
    drawerIcon:{
        paddingRight:10,
        width:30,
        height:33,
        marginRight:15
    },
    container:{
        flex:1,
        backgroundColor:'white'
    },
    indicatorBackground:{
        position:'absolute',
        backgroundColor:'rgba(0,0,100,0.8)',
        zIndex:10,
        width:width(100),
        height:height(100)
    },
    indicator:{
        top:300,
        zIndex:20
    },
    button: {
        backgroundColor: "lightblue",
        padding: 12,
        margin: 16,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    bottomModal: {
        justifyContent: "flex-end",
        marginTop: 300,
        marginLeft:0,
        marginRight:0,
        marginBottom:0
    },
    modalContent: {
        height:350,
        flex:1,
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    calendarModal:{
        marginTop: 100,
        marginLeft:10,
        marginRight:10,
        marginBottom:120
    },
    hiddenHeader:{
        width:width(100),
        position:'absolute',
        top:0,
        right:0,
        left:0,
        backgroundColor:'#ffefa6',
        zIndex:10
    },
    hiddenHeaderText:{
        alignSelf:'flex-start',        
        top:0,
        left:10,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontSize:15,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
    },
    HiddenSliderConterView:{
        elevation:10,
        position:'absolute',
        top:0,
        right:20
    },
    hiddenSliderConter:{
        backgroundColor:'#f4b642',
        width:50,
        height:50,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderWidth:2,
        borderColor:'#fff',
        borderRadius:50,
    },
    hiddenSliderConterIcon:{
        position:'absolute',
        top:36,
        color:'#f4b642',
        alignSelf:'center'
    },
    hiddenSliderCounterText:{
        top:2,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#fff',
        fontSize:24,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
    },
    ScrollView:{
        flexGrow:1,
    },
    ScrollViewContent:{
        flex: 1,
    },
    Footer:{
        height:20,
    },
    Header:{
        position:'absolute',
        top:0,
        right:0,
        left:0
    },
    headerImage:{
        width:width(60),
        height:height(20),
        resizeMode:'cover',
        alignSelf:'center',
        zIndex:-1000,
        
    },
    sliderContiner:{
        position:'absolute',
        top:height(20),
        right:0,
        left:0,

    },
    sliderConterView:{
        position:'absolute',
        top:0,
        left:0,

    },
    sliderConter:{
        width:44,
        height:44,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderRadius:50,
        borderWidth:2,
        borderColor:'rgba(255,255,255,1)'

    },
    sliderCounterText:{
        top:0,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#fff',
        fontSize:22,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)'
        
    },
    sliderCounterTextIcon:{
        zIndex:100,
        elevation:100,
        position:'absolute',
        top:30,
        color:'#f4b642',
        alignSelf:'center'
    },
    slider:{
        alignSelf:'center',
        top:35,
        position:'absolute'
    },
    sliderHiden:{
        alignSelf:'center',
        top:-10,
        position:'absolute'
    },
    sliderContentView:{
        position:'absolute',
        top:53,
        left:0,
    },
    sliderContent:{
        alignContent:'center',        
        width:95,
        height:40,
        alignItems: 'center',
        alignSelf:'flex-end',
        borderWidth:2,
        borderColor:'#fff',
        backgroundColor:'#999',
        borderBottomLeftRadius:17,
        borderTopLeftRadius:20,
        borderBottomRightRadius:17,
        borderTopRightRadius:20,
    },
    sliderContentIcons:{
        position:'absolute',
        top:-10,
        color:'#42d4f4',
        alignSelf:'center'
    },
    sliderCountentText:{
        top:4,
        right:3,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#fff',
        fontSize:9,
        fontWeight:'bold',
        backgroundColor:Platform.os === 'ios' ? 'rgba(250,250,250,0)':'rgba(250,250,250,0)',
        justifyContent:'center',
        alignSelf:'center',
        alignItems:'center',
    },
    subHeader:{
        left:10,
        alignSelf:'center',
        flexDirection:'row',
        top:60,
        zIndex:-100
    },
    subHeaderImage:{
        top:0,
        width:70,
        height:70,
    },
    subHeaderText:{
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontSize:12,
        width:width(70),
        paddingRight:20,
        paddingLeft:15,
        paddingTop:8,
        paddingBottom:8,
        marginTop:0,
        marginRight:10,
        borderWidth:2,
        borderColor:'#665',
        borderBottomLeftRadius:50,
        borderTopLeftRadius:50,
        borderBottomRightRadius:50,
        borderTopRightRadius:50,
    },



});