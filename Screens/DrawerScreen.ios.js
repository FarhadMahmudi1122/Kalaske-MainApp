import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {AppRegistry,Image, ScrollView, Text, View, StyleSheet} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { width, height, totalSize } from 'react-native-dimension';

class SideMenu extends Component {
    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    };

    render () {
        return (
            <View style={[styles.container]}>
                <View style={[styles.header]}>
                    <Image source={require("../Assets/Images/register_bg.png")} style={[styles.sideMenuImage]}/>
                    <MaterialIcons
                        style={[styles.cameraIcon]}
                        name={'photo-camera'}
                        size={20}
                    />
                    <MaterialIcons
                        style={[styles.profileIcon]}
                        name={'face'}
                        size={70}
                    />
                </View>
                <ScrollView>
                    <View style={[styles.Screens]}>
                        <Text style={[styles.textScreens]} onPress={this.navigateToScreen('DrawerProfile')}>پروفایل من</Text>
                    </View>
                    <View style={[styles.Screens]}>
                        <Text style={[styles.textScreens]} onPress={this.navigateToScreen('DrawerHome')}>خانه</Text>
                    </View>
                    <View style={[styles.Screens]}>
                        <Text style={[styles.textScreens]} onPress={this.navigateToScreen('DrawerInterests')}>اعلانات</Text>
                    </View>
                    <View style={[styles.Screens]}>
                        <Text style={[styles.textScreens]} onPress={this.navigateToScreen('DrawerProfile')}>درباره ما</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



SideMenu.propTypes = {
    navigation: PropTypes.object
};

export default SideMenu;

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    header:{
        position:'relative',
        marginBottom:30
    },
    sideMenuImage:{
        width: width(100),
        height: 200
    },
    profileIcon:{
        borderRadius:43,
        padding:10,
        color:'#000',
        backgroundColor:'#fff',
        position:'absolute',
        top:20,
        right:18,
        overflow:'hidden'

    },
    cameraIcon:{
        borderRadius:12,
        padding:3,
        color:'#000',
        backgroundColor:'gray',
        position:'absolute',
        top:80,
        right:20,
        zIndex:100,
        overflow:'hidden'

    },
    Screens:{
        margin:10,
        paddingRight:35,
        borderBottomWidth:0.5,
        borderColor:'gray',
    },
    textScreens:{
        color:'#000',
        fontSize:20,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        alignSelf:'flex-end',

    },



});

AppRegistry.registerComponent('SideMenu',() => SideMenu);