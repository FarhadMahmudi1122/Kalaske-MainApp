import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { bindActionCreators } from 'redux';
import {
  AppRegistry,
  Image,
  ScrollView,
  Text,
  View,
  StyleSheet,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { width, height, totalSize } from 'react-native-dimension';
import * as UserActions from '../actions/userActions';

var ImagePicker = require('react-native-image-picker');

function mapStateToProps({userInfo}){
  return{
      motherEmail:userInfo.motherEmail ? userInfo.motherEmail : 'You@gmail.com',
      weekNumber:userInfo.weekNumber,
      avatarSource:userInfo.avatarSource
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(UserActions, dispatch)
}

@connect(mapStateToProps, mapDispatchToProps)
class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      motherEmail:this.props.motherEmail,
      weekNumber:this.props.weekNumber,
      avatarSource:this.props.avatarSource
    }
    this._handelImagePicker = this._handelImagePicker.bind(this);
  }
  
  // Dispatch Functions
  setAvatarSource(avatarSource){
    this.props.setAvatarSource(avatarSource)
  }

  _handelImagePicker() {
    let options = {
      title: 'عکس خود را انتخاب کنید',
      takePhotoButtonTitle: 'عکس با دوربین',
      chooseFromLibraryButtonTitle: 'انتخاب از گالری',
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let avatarSource = { uri: response.uri };
        this.setAvatarSource(avatarSource)
      }
    });

  }

  _handleProfileSourceImage() {
    if (this.props.avatarSource !== null) {
      return this.props.avatarSource
    } else {
      return require('../Assets/Images/Profileicon.png');
    }

  }

  _handleProfileImageStyle() {
    if (this.props.avatarSource !== null) {
      return styles.profileImagePicked;

    } else {
      return styles.profileIconImage;
    }

  }

//......................Handle Responsive Style......................

  _handleNavigationTextFontSize(){
    if(height(100)<600){
      return(16)
    }else{
      return(20)
    }
  }

  _handleVersionTextFontSize(){
    if(height(100)<600){
      return(13)
    }else{
      return(16)
    }
  }

  _handleEmailFontSize(){
    if(height(100)<600){
      return(13)
    }else{
      return(16)
    }
  }

  _handleWeekNumberFontSize(){
    if(height(100)<600){
      return(11)
    }else{
      return(13)
    }
  }

  render() {
    const {motherEmail,weekNumber} = this.props;
    return (
      <View style={[styles.container]}>
        <View style={[styles.header]}>
          <View style={[styles.versionView]}>
            <Text style={[styles.versionText,{
              fontSize:this._handleVersionTextFontSize()
            }]}>نسخه</Text>
            <Text style={[styles.versionText,{
              fontSize:this._handleVersionTextFontSize()
            }]}>0.1.5</Text>
          </View>
          <Image
            source={require('../Assets/Images/DrawerHederBackground.jpg')}
            style={[styles.sideMenuImage]}/>
          <TouchableOpacity
            style={[styles.cameraIcon]}
            onPress={this._handelImagePicker}
          >
            <MaterialIcons
              name={'photo-camera'}
              size={20}
            />
          </TouchableOpacity>
          <View style={[styles.profileIcon]}>
            <Image
              resizeMode='contain'
              source={this._handleProfileSourceImage()}
              style={this._handleProfileImageStyle()}
            />

          </View>
          <View style={styles.emailWrapper}>
            <Text style={[styles.email,{
              fontSize:this._handleEmailFontSize()
            }]}>{motherEmail}</Text>
            <Text style={[styles.weekNumber,{
              fontSize:this._handleWeekNumberFontSize()
            }]}>{` شما در هفته ${weekNumber} بارداری هستید `}</Text>
          </View>
        </View>
        <ScrollView>
          <TouchableOpacity
            style={[styles.Screens, { marginTop: 50, }]}
            onPress={() => {
              this.props.navigation.navigate('DrawerProfile');
            }}
          >
            <Text style={[styles.textScreens,{
              fontSize:this._handleNavigationTextFontSize()
            }]}>پروفایل من</Text>
            <Image
              resizeMode='contain'
              source={require('../Assets/Images/Profileicon.png')}
              style={[styles.drawerIcon]}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.Screens]}
            onPress={() => {
              this.props.navigation.navigate('DrawerHome');
            }}
          >
            <Text style={[styles.textScreens,{
              fontSize:this._handleNavigationTextFontSize()
            }]}>خانه</Text>
            <Image
              resizeMode='contain'
              source={require('../Assets/Images/Homeicon.png')}
              style={[styles.drawerIcon]}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.Screens]}
            onPress={()=> this.props.navigation.navigate('DrawerNotification')}
          >
            <Text style={[styles.textScreens,{
              fontSize:this._handleNavigationTextFontSize()
            }]}>اعلانات</Text>
            <Image
              resizeMode='contain'
              source={require('../Assets/Images/Notification.png')}
              style={[styles.drawerIcon]}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.Screens]}
            onPress={()=> this.props.navigation.navigate('DrawerAboute')}
          >
            <Text style={[styles.textScreens,{
              fontSize:this._handleNavigationTextFontSize()
            }]}>درباره ما</Text>
            <Image
              resizeMode='contain'
              source={require('../Assets/Images/Aboute.png')}
              style={[styles.drawerIcon]}
            />
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}


SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  versionView:{
    position:'absolute',
    top:30,
    zIndex:10,
    left:30
  },
  versionText:{
    textAlign:'right',
    fontWeight:'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile'
  },
  header: {
    position: 'relative'
  },
  sideMenuImage: {
    width: width(100),
    height: height(30)
  },
  profileIcon: {
    borderRadius: width(20),
    padding: 10,
    backgroundColor: '#fff',
    position: 'absolute',
    top: 20,
    right: 18,
    width: width(20),
    height: width(20),
    overflow: 'hidden',
    backgroundColor: '#ddd',

  },
  profileIconImage: {
    width: width(20),
    height: width(20),
    right: width(3),
    top: -7,

  },
  profileImagePicked: {
    width: 130,
    height: 100,
    alignSelf: 'center',
    top: -25
  },
  cameraIcon: {
    padding: 3,
    position: 'absolute',
    top: 74,
    right: 20,
    zIndex: 100,
  },
  emailWrapper: {
    position: 'absolute',
    top: height(20),
    alignSelf: 'flex-end',
    paddingRight: 20
  },
  email: {
    right:4,
    textAlign:'right',
    color: '#fff',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile'
  },
  weekNumber: {
    textAlign:'right',    
    top: -5,
    color: '#fff',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile'
  },
  Screens: {
    marginTop: 10,
    paddingRight: 35,
    flexDirection: 'row'
  },
  drawerIcon: {
    width: 50,
    height: 50,
    top: 5
  },
  textScreens: {
    color: '#000',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',
    borderBottomWidth: 0.5,
    borderColor: 'gray',
    width: width(60),
    paddingRight: 15,
  },


});

AppRegistry.registerComponent('SideMenu', () => SideMenu);