import React from 'react';
import { bindActionCreators } from 'redux';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
  Keyboard,
  TextInput,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import Modal from 'react-native-modal';
import JalaliCalendarPicker from 'react-native-jalali-calendar-picker';
import Picker from 'react-native-picker';
import moment from 'moment-jalaali';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';
import * as UserActions from '../actions/userActions';



function mapStateToProps({userInfo}){
  return{
    weekNumber:userInfo.weekNumber,
    avatarSource:userInfo.avatarSource,
    motherDay:userInfo.motherDay,
    motherMonth:userInfo.motherMonth,
    motherYear:userInfo.motherYear,
    avatarSource:userInfo.avatarSource,
    motherName:userInfo.motherName,
    motherHeight:userInfo.motherHeight,
    motherWeight:userInfo.motherWeight,
    selectedStartDate:userInfo.selectedStartDate,
    childbirthDays:userInfo.childbirthDays,
    childbirthDateFormatted:userInfo.childbirthDateFormatted,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(UserActions, dispatch)
}

@connect(mapStateToProps, mapDispatchToProps)
export default class MotherProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      childbirthDays:this.props.childbirthDays,
      childbirthDateFormatted:this.props.childbirthDateFormatted,
      selectedStartDate: this.props.selectedStartDate,
      weekNumber:this.props.weekNumber,
      avatarSource:this.props.avatarSource,
      motherDay:this.props.motherDay,
      motherMonth:this.props.motherMonth,
      motherYear:this.props.motherYear,
      avatarSource:this.props.avatarSource,
      motherName:this.props.motherName,
      motherHeight:this.props.motherHeight,
      motherWeight:this.props.motherWeight,

    };

    this.onDateChange = this.onDateChange.bind(this);
    this._handelHeightPicker = this._handelHeightPicker.bind(this);
    this._handelWeightPicker = this._handelWeightPicker.bind(this);
    this._handleSelectedStartDateForBirth = this._handleSelectedStartDateForBirth.bind(this);
    this._handelDatePicker = this._handelDatePicker.bind(this);
    this._handelImagePicker = this._handelImagePicker.bind(this);
    this._handleProfileSourceImage = this._handleProfileSourceImage.bind(this);
    this._handleProfileImageStyle = this._handleProfileImageStyle.bind(this);
    this._handleDipatchInfo = this._handleDipatchInfo.bind(this);
  }

  //Dispatch Functions
  incrementChildBirthDays(childbirthDays){
    this.props.incrementChildBirthDays(childbirthDays);
  }
  
  setAvatarSource(avatarSource){
    this.props.setAvatarSource(avatarSource)
  }

  setUserInfoFromProfilePage(
    childbirthDays,
    childbirthDateFormatted,
    motherDay,
    motherMonth,
    motherYear,
    avatarSource,
    motherEmail,
    motherName,
    motherHeight,
    motherWeight,
    weekNumber,
    selectedStartDate
  ){
    this.props.setUserInfoFromProfilePage(
      childbirthDays,
      childbirthDateFormatted,
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherEmail,
      motherName,
      motherHeight,
      motherWeight,
      weekNumber,
      selectedStartDate
    );
  }

  componentDidMount(){
    this.incrementalChildBirthDays();
  }

  incrementalChildBirthDays(){
    const { selectedStartDate } = this.props;
    if (selectedStartDate){
      childbirthDays = (Math.ceil(Math.ceil(
        (moment(selectedStartDate).valueOf()-moment().valueOf()) /
        (1000 * 60 * 60 * 24))))  
        this.incrementChildBirthDays(childbirthDays);
    }

  }



  _handelWeightPicker() {
    weightData = [];
    for (var i = 40; i < 150; i++) {
      weightData.push(i);
    }

    const setWeight = (motherWeight) => {
      this.setState({ motherWeight })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: weightData,
      selectedValue: [this.props.motherWeight],
      onPickerConfirm: data => {
        setWeight(parseInt(data[0], 10))
      }
    });
    Picker.show();
  };

  _handelHeightPicker() {
    heightData = [];
    for (var i = 140; i < 200; i++) {
      heightData.push(i);
    }

    const setHeight = (motherHeight) => {
      this.setState({ motherHeight })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: heightData,
      selectedValue: [this.props.motherHeight],
      onPickerConfirm: data => {
        setHeight(parseInt(data[0], 10))
      }
    });
    Picker.show();
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }

  _handelImagePicker() {
    let options = {
      title: 'عکس خود را انتخاب کنید',
      takePhotoButtonTitle: 'عکس با دوربین',
      chooseFromLibraryButtonTitle: 'انتخاب از گالری',
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let avatarSource = { uri: response.uri };
        this.setAvatarSource(avatarSource)
        this.setState({avatarSource:avatarSource})
      }
    });
  }

  _handelDatePicker() {
    dateData = pickerData = [
      [1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376],
      ['مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند', 'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور'],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
    ];


    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: dateData,
      selectedValue: [this.props.motherYear, this.props.motherMonth, this.props.motherDay],
      onPickerConfirm: data => {
        this.setState({
          motherYear: data[0],
          motherMonth: data[1],
          motherDay: data[2],
        })
      }
    });
    Picker.show();
  }

  _handleProfileSourceImage() {
    if (this.props.avatarSource !== null) {
      return this.props.avatarSource
    } else {
      return require('../Assets/Images/Profileicon.png');
    }

  }

  _handleProfileImageStyle() {
    if (this.props.avatarSource ) {
      return styles.profileImagePicked;

    } else {
      return styles.profileImage;
    }

  }

  _handleSelectedStartDateForBirth() {
    const { selectedStartDate } = this.state;

    if ( selectedStartDate !== null ) {
      const childbirthDateFormatted = selectedStartDate ? selectedStartDate.format('jYYYY/jM/jD') : null;
      const childbirthDays = (Math.ceil(Math.ceil((this.state.selectedStartDate.valueOf() - moment().valueOf()) / (1000 * 60 * 60 * 24))));
      const weekNumber = 40 - (Math.ceil(Math.ceil((this.state.selectedStartDate.valueOf()-moment().valueOf()) / (1000 * 60 * 60 * 24))/7)-1);
      this.setState({
        visibleModal: null,
      });
      this.setState({
        childbirthDays,
        childbirthDateFormatted,
        weekNumber,
      });
    } else {
      this.setState({
        visibleModal: null,
      })
    }
  }

  _handleChangeNameInput(motherName) {
    this.setState({motherName})      
  }

  _handleDipatchInfo() {
    const {
      childbirthDays,
      childbirthDateFormatted,
      avatarSource,
      motherDay,
      motherMonth,
      motherYear,
      motherName,
      motherHeight,
      motherWeight,
      weekNumber,
      selectedStartDate
    } = this.state;

    this.setUserInfoFromProfilePage(
      childbirthDays,
      childbirthDateFormatted,
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherName,
      motherHeight,
      motherWeight,
      weekNumber,
      selectedStartDate
    );

  }


//......................Handle Responsive Style......................


  _handleInputLableFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  _handleInputlableMarginBottom(){
    if(height(100)<600){
      return(-20)
    }else{
      return(-15)
    }
  }

  _handleInputFontSize(){
    if(height(100)<600){
        return(11)
    }else{
        return(null)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(70)
    }else{
        return(100)
    }
  }

  _handleButtonTextFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  _handleProfileImagePoition(){
    if(height(100)<600){
      return(-height(13))
    }else{
      return(-height(10))
    }
  }

  _handleSubHeaderTopMarginTop(){
    if(height(100)<600){
      return(10)
    }else{
      return(height(8))
    }
  }

  render() {
    //states for component itself
    const {
      selectedStartDate,      
      childbirthDays,
      childbirthDateFormatted,
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherEmail,
      motherName,
      motherHeight,
      motherWeight
    } = this.state;

    const startDate = selectedStartDate ? moment(selectedStartDate).format('jYYYY/jM/jD') : '' ;

    return (

      <ScrollView style={[styles.container]}>
          <View style={[styles.headerBar]}>
              <Text style={[styles.topHeaderRight]}> پروفایل مادر و کودک </Text>
          </View>
        <Modal
          style={styles.calendarModal}
          onBackdropPress={() => this.setState({ visibleModal: false })}
          isVisible={this.state.visibleModal === 2}
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          animationInTiming={800}
          animationOutTiming={1000}
          backdropTransitionInTiming={800}
          backdropTransitionOutTiming={800}
          useNativeDriver={true}
        >
          <View style={styles.modalContent}>
            <JalaliCalendarPicker
              minDate={new Date((new Date()).valueOf() + 1000 * 3600 * 24)}
              onDateChange={this.onDateChange}
              width={width(100)}
              height={height(55)}
            />
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{`تاریخ انتخابی شما: ${startDate}`}</Text>
            <TouchableOpacity onPress={this._handleSelectedStartDateForBirth}>
              <View style={[styles.button, { width: 100, height: 50 }]}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>تائید</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
        <Image
          source={require('../Assets/Images/headerBig.jpg')}
          style={[styles.headerImage, { zIndex: -20 }]}
        />
        <View style={{ 
          top: this._handleProfileImagePoition() 
          }}>
          <View style={[styles.profileImageView]}>
            <Image
              resizeMode='contain'
              style={this._handleProfileImageStyle()}
              source={this._handleProfileSourceImage()}
            />
          </View>
          <TouchableOpacity
            style={[styles.profileImageBtn]}
            onPress={this._handelImagePicker}
          >
            <MaterialIcons
              style={{ alignSelf: 'center', }}
              name={'photo-camera'}
              size={width(6)}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.subHeaderSection]}>
          <View style={[styles.subHeaderTop,{
            marginTop: this._handleSubHeaderTopMarginTop()
          }]}>
            <Text style={[styles.subHeaderTopLeftText]}
            >{childbirthDays} روز تا زایمان</Text>
            <Text style={[styles.subHeaderTopRightText]}
            >{this.state.motherName}</Text>
          </View>
          <View style={[styles.subHeaderBottom]}>
            <TouchableOpacity
              onPress={() => {
                this.setState({ visibleModal: 2 })
              }}
            >
              <MaterialIcons
                style={[styles.subHeaderBottomEditIcon]}
                name={'edit'}
                size={20}
              />
            </TouchableOpacity>
            <Text style={[styles.subHeaderBottomLeftText]}
            >{childbirthDateFormatted}</Text>
            <Text
              style={[styles.subHeaderBottomRightText]}
            >تاریخ زایمان</Text>
          </View>
        </View>
        <View>
          <View>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize(),
              marginBottom:this._handleInputlableMarginBottom()
            }]}>نام</Text>
            <TextInput
              onChangeText={(value)=>this._handleChangeNameInput(value)}
              underlineColorAndroid={'#bcbcbc'}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'نام خود را وارد کنید'}
              value={`${motherName}`}/>
          </View>
          <TouchableOpacity onPress={this._handelDatePicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize(),
              marginBottom:this._handleInputlableMarginBottom()
            }]}>تاریخ تولد مادر</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={`روز ماه سال`}
              value={`${motherDay} ${motherMonth} ${motherYear}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelWeightPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize(),
              marginBottom:this._handleInputlableMarginBottom()
            }]}>وزن پیش از زایمان</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'وزن'}
              value={`${motherWeight}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelHeightPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize(),
              marginBottom:this._handleInputlableMarginBottom()
            }]}>قد</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'قد'}
              value={`${motherHeight} سانتیمتر`}/>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
          <TouchableOpacity
            onPress={() => {
              this._handleDipatchInfo();
              this.props.navigation.goBack()
              }}
            style={[styles.button,{
              width:this._handleButtonWidth()
            }]}
          >
            <Text style={[styles.buttonText,{
              fontSize:this._handleButtonTextFontSize()
            }]}>ذخیره تغیرات</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this._handleDipatchInfo()
              this.props.navigation.navigate('BabyProfile');
            }}
            style={[styles.enterbutton,{
              width:this._handleButtonWidth()
            }]}
          >
            <Text style={[styles.buttonText,{
              fontSize:this._handleButtonTextFontSize()
            }]}>ادامه</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.Footer]}>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({

  topHeaderRight: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#000',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf:'flex-end'
    
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  headerBar:{
      height:35,
      backgroundColor: 'white',
      elevation: 5,
  },
  modalContent: {
    margin: 10,
    height: 350,
    flex: 1,
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  calendarModal: {
    marginTop: height(14),
    marginLeft: width(1),
    marginRight: width(1),
    marginBottom: height(14)
  },
  headerImage: {
    width: width(100),
    height: height(25)
  },
  header: {
    alignItems: 'center',
    marginTop: 5,
  },
  profileImageView: {
    width: width(25),
    height: height(14),
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: '#ddd',
    borderRadius: 50, 
    overflow: 'hidden' 
  },
  profileImage: {
    width: width(25),
    height: height(15),
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: '#ddd'
  },
  profileImagePicked: {
    width: 160,
    height: 110,
    alignSelf: 'center',
    top: -10
  },
  profileImageBtn: {
    width: width(6),
    height: height(6),
    borderRadius: 15,
    alignSelf: 'center',
    top: -20,
    left: 30,
    alignItems: 'center',
    zIndex: 100
  },
  subHeaderSection: {
    marginTop: -height(22),
    backgroundColor: '#eee',
    zIndex: -10
  },
  subHeaderTop: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  subHeaderTopLeftText: {
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  subHeaderTopRightText: {
    width: width(35),
    marginLeft: width(15),
    marginRight: width(10),
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  subHeaderBottom: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginBottom: 20
  },
  subHeaderBottomEditIcon: {
    color: '#f4b642',
    marginRight: 10
  },
  subHeaderBottomLeftText: {
    marginRight: width(0),
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  subHeaderBottomRightText: {
    width: width(35),
    marginLeft: width(15),
    marginRight: width(10),
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  inputLable: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',
    paddingRight: 18,
  },
  textInputStyle: {
    textAlign: 'right',
    width: width(95),
    alignSelf: 'center',
  },
  touchableInputLable: {
    borderBottomWidth: 2,
    borderColor: 'gray'
  },
  touchableInputLableText: {},
  button: {
    backgroundColor: '#f4b642',
    height: 40,
    marginTop: 5,
    borderRadius: 14,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center'

  },
  enterbutton: {
    backgroundColor: '#f4b642',
    height: 40,
    marginLeft: 50,
    marginTop: 5,
    borderRadius: 14,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',

  },
  Footer: {
    height: 20,
  },


});
