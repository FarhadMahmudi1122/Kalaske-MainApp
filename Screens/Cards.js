import React, { Component } from "react";
import { Text,
    TouchableOpacity,
    View,
    StyleSheet ,
    Platform,
    Image,
    ToastAndroid
    } from "react-native";
import { width, height, totalSize } from 'react-native-dimension';
import { connect } from 'react-redux'
import DefaultCardData from './DefaultData/DefaultCardData'

function mapStateToProps({userInfo , registrationInfo}){
    return {
        weekNumber: userInfo.weekNumber,
        isRegistered: registrationInfo.isRegistered
        
    }
   }
   
@connect(mapStateToProps)
export default class RenderCards extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            
        }
    }

    //......................Handle Responsive Style......................


    _handleHeaderFontSize(){
        if(height(100)<600){
            return(14)
        }else{
            return(18)
        }
    }

    _handleSubHeaderFontSize(){
        if(height(100)<600){
            return(10)
        }else{
            return(13)
        }
    }

    _handleDiscriptionFontSize(){
        if(height(100)<600){
            return(10)
        }else{
            return(13)
        }
    }


    render(){
        const {
            changeablevalue,
            cards,
            weekNumber,
            isRegistered,
            noConnectionError,
            defaultData
        } = this.props;
 
        if(!noConnectionError&&isRegistered &&
            (weekNumber !==0 ) && ((weekNumber - 3) < changeablevalue) && (changeablevalue <= weekNumber) ||
            !noConnectionError&&changeablevalue === 0){
            let renderCards = cards.map((card,index)=>{
            let cardImageUrl = 'http:/rafshadow.ir'+(card.Media[0].Url);
            if(card.FullContent.ContentMedium.Type === 'video/mp4'){
                return (
                    <TouchableOpacity
                        key={index}
                        onPress={() => this.props.navigation.navigate('Info',{
                            card:card,

                        })}
                        style={[styles.listTouchable]}
                        activeOpacity={0.8}
                    >
                        <Image 
                            resizeMode="contain" 
                            style={[styles.listImageWithVideo]} 
                            source={{uri:cardImageUrl}}
                        />
                        <View style={[styles.listViewContentWithVideo]}>
                            <Text style={[styles.listViewHeader,{
                                fontSize:this._handleHeaderFontSize(),
                                }]}>{card.Header}
                            </Text>
                            <Text 
                                numberOfLines={1}
                                style={[styles.listViewSubHeader,{
                                    fontSize:this._handleSubHeaderFontSize(),
                                }]}
                            >
                                {card.Subheader}
                            </Text>
                            <Text 
                                numberOfLines={3}
                                style={[styles.listViewDiscription,{
                                    fontSize:this._handleDiscriptionFontSize(),
                                }]}
                            >
                                {card.Description}
                            </Text>
                        </View>
                        <View 
                            style={[styles.listViewColorSection,{
                                backgroundColor:card.SubjectCategory.Color
                                }]}
                            >
                        </View>
                    </TouchableOpacity>
                    )} else {
                        return (
                            <TouchableOpacity
                            key={index}
                            onPress={() => this.props.navigation.navigate('Info',{
                                card:card,
                            })}
                            style={[styles.listTouchable]}
                            activeOpacity={0.8}
                        >
                            <Image 
                                resizeMode="contain" 
                                style={[styles.listImage]} 
                                source={{uri:cardImageUrl}}
                            />
                            <View style={[styles.listViewContent]}>
                                <Text style={[styles.listViewHeader,{
                                    fontSize:this._handleHeaderFontSize()
                                }]}>{card.Header}</Text>
                                <Text 
                                    numberOfLines={1}
                                    style={[styles.listViewSubHeader,{
                                        fontSize:this._handleSubHeaderFontSize(),
                                    }]}
                                >
                                    {card.Subheader}
                                </Text>
                                <Text 
                                    style={[styles.listViewDiscription,{
                                        fontSize:this._handleDiscriptionFontSize(),
                                    }]}
                                    numberOfLines={3}
                                >
                                    {card.Description}
                                </Text>
                            </View>
                            <View 
                                style={[styles.listViewColorSection,{
                                    backgroundColor:card.SubjectCategory.Color,
                                    }]}
                                >
                            </View>
                        </TouchableOpacity>
                        )
                    }
                });
            return(
                <View>{renderCards}</View>
            )
        } else if (!noConnectionError&&isRegistered &&
                    (weekNumber!==0) && (changeablevalue <= (weekNumber - 3)) || 
                    !noConnectionError&&isRegistered && (weekNumber !== 0) && weekNumber < changeablevalue || 
                    !noConnectionError&&isRegistered &&
                    (weekNumber === 0) && weekNumber < changeablevalue) {
                return (
                    <View style={[styles.changeProfileView]}>
                    <Text style={[styles.changeProfileText]}>با توجه به تاریخ زایمان‌تان، مطالب این هفته برای شما باز نشده است. برای اصلاح تاریخ زایمان به صفحه‌ی پروفایل بروید.</Text>
                    </View>
                )
        } else if(isRegistered && noConnectionError && changeablevalue < weekNumber && 
                    (weekNumber-3)<changeablevalue || 
                    noConnectionError && changeablevalue===0) {
                    if(defaultData){
                        return (
                            <DefaultCardData/>
                        )
                    } else {
                        return (
                            <View style={{marginTop:20,padding:20}}>
                                <Text style={[styles.noConnectionText]}>
                                    عدم ارتباط با سرور 
                                </Text>
                            </View>
                        )
                    }

        } else {
            return (
                <View>
                    <View style={[styles.registrationView]}>
                    <Text style={[styles.registrationText]}>لطفا جهت مشاهده محتوای مربوط به هفته بارداری خود عضو سرویس شوید</Text>
                    </View>
                    <TouchableOpacity 
                        style={[styles.registrationButton]}
                        onPress={()=>{this.props.navigation.replace('LogInFirst')}}
                    >
                    <Text style={[styles.registrationButtonText]}>برای عضویت اینجا را لمس کنید</Text>
                    </TouchableOpacity>
                </View>
                
            )
        }
    }      
}



const styles = StyleSheet.create({
    listTouchable:{
        flexDirection:'row',
        alignItems:'center',
        height:height(24),
        borderColor:'#ddd',
        borderBottomWidth:0.8
    },
    listViewContent:{
        width:width(65),
        marginRight:width(1),
        marginLeft:4
    },
    listViewHeader:{
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontWeight:'bold',
    },
    listViewSubHeader:{
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#999',
    },
    listViewDiscription:{
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
    },
    listViewColorSection:{
        width:width(3),
        height:height(22),

    },
    listImage:{
        width:width(30),
        height:height(30),
        top:10,
        left:7,
    },
    listTranceImage:{
        width:width(50),
        position:'absolute',
        left:-10,
        zIndex:10
    },
    listImageWithVideo:{
        width:width(31),
        height:height(31),      
        left:0,
        top:8
    },
    listViewContentWithVideo:{
        width:width(65),
        marginRight:width(1),
        marginLeft:width(0)
    },
    changeProfileText:{
        marginTop:30,
        marginBottom:30,        
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#42d4f4',
        fontSize:20,
        fontWeight:'bold',
        textAlign:'center'
    },
    noConnectionText:{
        alignSelf:'center',
        color:'#42d4f4',
        fontSize:20,
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
    },
    changeProfileView:{
        marginTop:20,
        padding:20
    },
    registrationView:{
        marginTop:20,
        padding:20
    },
    registrationText:{
        marginTop:30,
        marginBottom:30,        
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#42d4f4',
        fontSize:20,
        fontWeight:'bold',
        textAlign:'center'
    },
    registrationButton:{
        alignItems:'center'
    },
    registrationButtonText:{
        textAlign:'center',
        color:'blue',
        borderBottomWidth:1,
        borderColor:'#000',
        width:210,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontWeight:'bold',
        fontSize:15,
        
    }
});


