import React from 'react';
import { bindActionCreators } from 'redux';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
  Keyboard,
  TextInput

} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import Picker from 'react-native-picker';
import { connect } from 'react-redux';
import * as UserActions from '../actions/userActions';

function mapStateToProps({ userInfo }) {
  return {
    babyInfo: {
      motherEmail:userInfo.motherEmail,
      babyNumber: userInfo.babyNumber,
      babyName: userInfo.babyName,
      babySex: userInfo.babySex
    }
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(UserActions, dispatch)
}

@connect(mapStateToProps, mapDispatchToProps)
export default class BabyProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.babyInfo);
    this.state = { ...this.props.babyInfo };

    this._handelBabyNumberPicker = this._handelBabyNumberPicker.bind(this);
    this._handelBabySexPicker = this._handelBabySexPicker.bind(this);

  }

  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      headerStyle: { height: 35 },
      headerRight: <Text style={[styles.topHeaderRight]}> پروفایل مادر و کودک </Text>,
      headerLeft: null
    }
  };

  //Dispatch Functions
  setBabyInfoFromFifthLogin(babyName, babySex, babyNumber){
    this.props.setBabyInfoFromFifthLogin(babyName, babySex, babyNumber)
  }

  setMotherEmailBabyProfile(motherEmail){
    this.props.setMotherEmailBabyProfile(motherEmail)
  }

  _handleEmailValue(inputValue) {
    this.setState({
      motherEmail: inputValue,
    })
  }

  _handelBabyNumberPicker() {
    babyNumberdata = [];
    for (var i = 0; i < 8; i++) {
      babyNumberdata.push(i);
    }

    //console.log(this);
    const setbaby = (babyNumber) => {
      this.setState({ babyNumber })
    }

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: babyNumberdata,
      selectedValue: [this.state.babyNumber],
      onPickerConfirm: data => {
        //console.log(data);
        setbaby(parseInt(data[0], 10))
      }
    });
    Picker.show();
  }

  _handelBabySexPicker() {
    const babySexData = ['دختر', 'پسر'];

    const setBabySex = (babySex) => {
      this.setState({ babySex })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: babySexData,
      selectedValue: [this.state.babySex],
      onPickerConfirm: data => {
        setBabySex(data[0])
      }
    });
    Picker.show();
  }

  _handleDipatchInfo() {
    const {
      motherEmail,
      babyName,
      babySex,
      babyNumber
    } = this.state;

    this.setBabyInfoFromFifthLogin(babyName, babySex, babyNumber);
    this.setMotherEmailBabyProfile(motherEmail);
    
  }

//......................Handle Responsive Style......................


  _handleInputLableFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  _handleInputlableMarginBottom(){
    if(height(100)<600){
      return(-20)
    }else{
      return(-15)
    }
  }

  _handleInputFontSize(){
    if(height(100)<600){
        return(11)
    }else{
        return(null)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(70)
    }else{
        return(100)
    }
  }

  _handleButtonTextFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  render() {
    return (
      <ScrollView style={[styles.container]}>
        <Image
          resizeMode='contain'
          source={require('../Assets/Images/Login3.jpg')}
          style={[styles.profileHeader]}
        />
        <View style={{ marginTop: 20 }}>
        <View>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>ایمیل</Text>
            <TextInput
              onChangeText={(value)=>this._handleEmailValue(value)}
              underlineColorAndroid={'#bcbcbc'}
              keyboardType='email-address'
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'ایمیل خود را وارد کنید'}
              value={`${this.state.motherEmail}`}/>
          </View>
          <View>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>نام انتخابی کودک</Text>
            <TextInput
              underlineColorAndroid={'#bcbcbc'}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              onChangeText={(value) => this.setState({ babyName: value })}
              placeholder={'برای فرزند خود چه نامی انتخاب کرده اید؟'}
              value={`${this.state.babyName}`}/>
          </View>
          <TouchableOpacity onPress={this._handelBabySexPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>جنسیت کودک</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'دختر یا پسر؟'}
              value={`${this.state.babySex}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelBabyNumberPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>فرزند چندم شماست</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={`${this.state.babyNumber}`}
              value={`${this.state.babyNumber}`}/>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
          <TouchableOpacity
            onPress={() => {
              this._handleDipatchInfo();
              this.props.navigation.replace('Home')
            }}
            style={[styles.button]}
          >
            <Text style={[styles.buttonText,{
              fontSize:this._handleButtonTextFontSize()
            }]}>ذخیره تغیرات</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.Footer]}>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({

  topHeaderRight: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 18,
    color: '#000',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  profileHeader: {
    width: width(100),
    height: height(30)
  },
  header: {
    alignItems: 'center',
    marginTop: 5,
  },
  headerText: {
    marginBottom: 5,
    marginTop: 5,
    marginRight: 20,
    paddingRight: 10,
    borderRightWidth: 3,
    borderRightColor: '#f4b642',
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    fontSize: 18,
    alignSelf: 'flex-end',

  },
  subHeaderText: {
    color: 'gray',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    fontSize: 14,
    textAlign: 'right',
    alignSelf: 'flex-end',
    marginLeft: 20,
    marginRight: 20,
    marginTop: -10,
    marginBottom: 20
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 500,
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#ccc'
  },
  profileImageBtn: {
    width: 30,
    height: 30,
    borderRadius: 15,
    alignSelf: 'center',
    top: -20,
    left: 30,
    alignItems: 'center',


  },
  inputLable: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',
    paddingRight: 18,
    marginBottom: -15
  },
  textInputStyle: {
    textAlign: 'right',
    width: width(95),
    alignSelf: 'center',
  },
  touchableInputLable: {
    borderBottomWidth: 2,
    borderColor: 'gray'
  },
  touchableInputLableText: {},
  button: {
    backgroundColor: '#f4b642',
    width: 100,
    height: 40,
    marginTop: 5,
    borderRadius: 14,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center'

  },
  enterbutton: {
    backgroundColor: '#f4b642',
    width: 100,
    height: 40,
    marginLeft: 50,
    marginTop: 5,
    borderRadius: 14,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',

  },
  Footer: {
    height: 20,
  },


});
