import React from 'react';
import {Text, View, StyleSheet, ScrollView, Image, Platform} from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import Slider from 'react-native-slider';


export default class AboutScreen extends React.Component {
    state = {
        value: 0
    };
    // static navigationOptions = ({ navigation, screenProps }) => {
    //     return {
    //     title:  '',
    //     headerStyle: {height: 35},
    //     headerTitleStyle:{
    //         color:'#000',
    //         fontWeight:'bold',
    //         fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
    //         textAlign: 'center',
    //         alignSelf:'center',
    //     },
    //     headerRight: <View style={[styles.headerBarRight]}>
    //         <Text
    //             style={[styles.headerBarText]}
    //             onPress={() => { navigation.navigate('Home')}} >در باره ما</Text>
    //         <MaterialIcons
    //             style={[styles.headerBarclearIcon]}
    //             name={'clear'}
    //             size={30}
    //             onPress={() => {navigation.navigate('Home')}}/></View>,}
    // };
    render(){
        return (
            <View style={[styles.container]}>
                <View style={[styles.headerBar]}>
                    <View style={[styles.headerBarRight]}>
                        <Text
                            style={[styles.headerBarText]}
                            onPress={() => { this.props.navigation.navigate('Home')}} >در باره ما</Text>
                        <MaterialIcons
                            style={[styles.headerBarclearIcon]}
                            name={'clear'}
                            size={30}
                            onPress={() => {this.props.navigation.navigate('Home')}}/>
                    </View>
                </View>
                <Image style={[styles.hederImage]} source={require('../Assets/Images/logo.png')}/>
                <Text style={[styles.hederText]}>کالسکه</Text>
                <Text style={[styles.conditionText]}>«کالسکه» اپلیکیشنی است برای همراهی و راهنمایی در دوران بارداری، مرجع پزشکی نیست. اگر بیماری خاصی دارید، قبل به‌کارگیری مطالب با پزشک خود مشورت کنید </Text>
                <View style={styles.emailView}>
                    <Text  style={styles.emailText}>info@kaaleskeh.ir</Text>
                    <MaterialIcons
                        style={[styles.emailIcon]}
                        name={'email'}
                        size={30}
                    />
                </View> 
                <View style={styles.phoneView}>
                    <Text  style={styles.phoneText}>تماس با ما</Text>
                    <MaterialIcons
                        style={[styles.phoneIcon]}
                        name={'phone'}
                        size={30}
                    />
                </View> 
                <Text style={[styles.footerText]}>کلیه حقوق کالسکه متعلق به شرکت رف است</Text>
                <Text style={[styles.usageText]}>شرایط استفاده</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'#fff'
    },
    headerBar:{
        height:35,
        backgroundColor: 'white',
        elevation: 5,
    },
    headerBarRight:{
        alignSelf: 'flex-end',
        flex:1,
        flexDirection: 'row',
    },
    headerBarText:{
        marginRight:0,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontSize:18,
        fontWeight:'bold',
    },
    headerBarclearIcon:{
        color:'#000',
    },
    hederImage:{
        marginTop:20,
        width:width(23),
        height:height(15),
        alignSelf:'center'
    },
    hederText:{
        marginTop:15,        
        alignSelf:'center',
        fontSize:28,
        color:'#000',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        
    },
    conditionText:{
        width:width(90),  
        marginTop:10,        
        alignSelf:'center',
        textAlign:'center',
        fontSize:14,
        color:'#000',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        
    },
    emailView:{
        flexDirection:'row',
        backgroundColor:'#f4b642',
        width:width(70),
        height:40,
        marginTop:20,
        borderRadius:15,
        alignSelf:'center',
        justifyContent: 'center',
        alignItems: 'center'

    },
    emailText:{
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:16,
        textAlign: 'center',

    },
    emailIcon:{
        color:'#000',
        marginLeft:10
        
    },
    phoneView:{
        flexDirection:'row',
        backgroundColor:'#f4b642',
        width:width(50),
        height:40,
        marginTop:10,
        borderRadius:15,
        alignSelf:'center',
        justifyContent: 'center',
        alignItems: 'center'

    },
    phoneText:{
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:16,
        textAlign: 'center',

    },
    phoneIcon:{
        color:'#000',
        marginLeft:10
        
    },
    footerText:{
        marginTop:height(15),
        color:'#000',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:14,
        textAlign: 'center',
    },
    usageText:{
        color:'red',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:14,
        textAlign: 'center',
    }
});