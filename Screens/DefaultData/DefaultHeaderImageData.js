import React from 'react';
import {
    Text, 
    View, 
    StyleSheet,
    ScrollView,
    Image,
    Platform,
    TouchableOpacity,
    Animated,
    ActivityIndicator,
    ToastAndroid
} from "react-native";
import { width, height, totalSize } from 'react-native-dimension';
import { connect } from 'react-redux'


export default class DefaultHeaderImageData extends React.Component {
    constructor (props) {
        super(props);
        this.state = {

        }
    }
    
    render(){

        const {
            headerImageUrl,
            scrollY,
            noConnectionError

        } = this.props

        const imageHeaderHeight = scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[height(20),0],
            extrapolate: 'clamp',
            useNativeDriver: true

        });
        const imageHeaderOpacity = scrollY.interpolate({
            inputRange: [110,210],
            outputRange:[1,0],
            extrapolate: 'clamp',
            useNativeDriver: true
        });

        if (noConnectionError){
            return(
                <Animated.View 
                    style={[styles.Header]}
                >
                    <Animated.Image 
                        resizeMode="contain" 
                        style={[styles.headerImage,{
                            height:imageHeaderHeight,
                            opacity:imageHeaderOpacity
                        }]} 
                        source={require('../../Assets/Images/headerBig.jpg')}
                    />
                </Animated.View>
            )


        } else {
            return(
                <Animated.View 
                    style={[styles.Header]}
                >
                    <Animated.Image 
                        resizeMode="contain" 
                        style={[styles.headerImage,{
                            height:imageHeaderHeight,
                            opacity:imageHeaderOpacity
                        }]} 
                        source={{uri:headerImageUrl}}
                    />
                </Animated.View>
            )
        }
    }
}


const styles = StyleSheet.create ({
    Header:{
        position:'absolute',
        top:0,
        right:0,
        left:0
    },
    headerImage:{
        width:width(60),
        height:height(20),
        resizeMode:'cover',
        alignSelf:'center',
        zIndex:-1000,
        
    }, 
})