import React, { Component } from "react";
import { Text,
    TouchableOpacity,
    View,
    StyleSheet ,
    Platform,
    Image,
    ToastAndroid
    } from "react-native";
import { width, height, totalSize } from 'react-native-dimension';
import { connect } from 'react-redux'


export default class DefaultCardData extends React.Component {
    constructor (props) {
        super(props);
        this.state = {

        }
    }

    _handleHeaderFontSize(){
        if(height(100)<600){
            return(14)
        }else{
            return(18)
        }
    }

    _handleSubHeaderFontSize(){
        if(height(100)<600){
            return(10)
        }else{
            return(13)
        }
    }

    _handleDiscriptionFontSize(){
        if(height(100)<600){
            return(10)
        }else{
            return(13)
        }
    }

    render(){
        return(
            <TouchableOpacity
            onPress={null}
            style={[styles.listTouchable]}
            activeOpacity={0.8}
        >
            <Image 
                resizeMode="contain" 
                style={[styles.listImageWithVideo]} 
                source={require('../../Assets/Images/Profileicon.png')}
            />
            <View style={[styles.listViewContentWithVideo]}>
                <Text style={[styles.listViewHeader,{
                    fontSize:this._handleHeaderFontSize(),
                }]}>
                    نطفه‌ی توی شکم‌تان چطور به وجود آمده؟
                </Text>
                <Text 
                    numberOfLines={1}
                    style={[styles.listViewSubHeader,{
                        fontSize:this._handleSubHeaderFontSize(),
                    }]}
                >
                    هفته‌ی سوم جنین 
                </Text>
                <Text 
                    numberOfLines={3}
                    style={[styles.listViewDiscription,{
                        fontSize:this._handleDiscriptionFontSize(),
                    }]}
                >
                    یك اتفاق خیلی مهم در بدن شما افتاده است: ملاقات اسپرم و تخمك
                </Text>
            </View>
            <View 
                style={[styles.listViewColorSection]}
                >
            </View>
        </TouchableOpacity>
        )
    }
}



const styles = StyleSheet.create ({

listTouchable:{
    flexDirection:'row',
    alignItems:'center',
    height:height(24),
    borderColor:'#ddd',
    borderBottomWidth:0.8
},
listImageWithVideo:{
    width:width(31),
    height:height(31),      
    left:0,
    top:8
},
listViewContentWithVideo:{
    width:width(65),
    marginRight:width(1),
    marginLeft:width(0)
},
listViewHeader:{
    fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
    color:'#000',
    fontWeight:'bold',
},
listViewSubHeader:{
    fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
    color:'#999',
},
listViewDiscription:{
    fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
    color:'#000',
},
listViewColorSection:{
    width:width(3),
    height:height(22),
    backgroundColor:'red'
},


})