import React, { Component } from "react";
import { Text,
    TouchableOpacity,
    View,
    StyleSheet ,
    Platform,
    Image,
    ToastAndroid
    } from "react-native";
import { width, height, totalSize } from 'react-native-dimension';
import { connect } from 'react-redux'


export default class DefaultSliderData extends React.Component {
    constructor (props) {
        super(props);
        this.state = {

        }
    }

    render(){
        const {
            sliderText,
            sliderImageUrl,
            noConnectionError,
        } = this.props;
        if(noConnectionError){
            return(

                <View style={[styles.subHeader]}>
                    <Text 
                        numberOfLines={4} 
                        ellipsizeMode={'tail'} 
                        style={[styles.subHeaderText]}
                    >
                        به «کالسکه» خوش آمدید. می‌خواهید بدانید این‌جا چه خبر است؟ نمونه‌های ویدئوها و متن‌های ما را ببینید. برای دسترسی به تمام محتوا ثبت نام کنید.

                    </Text>
                    <Image 
                        resizeMode="contain" 
                        style={[styles.subHeaderImage]} 
                        source={require('../../Assets/Images/Profileicon.png')} 
                    />
                </View>
            )
        } else {
            return(
                
                <View style={[styles.subHeader]}>
                    <Text 
                        numberOfLines={4} 
                        ellipsizeMode={'tail'} 
                        style={[styles.subHeaderText]}
                    >
                        {sliderText}
                    </Text>
                    <Image 
                        resizeMode="contain" 
                        style={[styles.subHeaderImage]} 
                        source={{uri:sliderImageUrl}} 
                    />
                </View>
            )
        }
    }


}


const styles = StyleSheet.create ({

    subHeader:{
        left:10,
        alignSelf:'center',
        flexDirection:'row',
        top:60,
        zIndex:-100
    },
    subHeaderImage:{
        top:0,
        width:70,
        height:70,
    },
    subHeaderText:{
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontSize:12,
        width:width(70),
        paddingRight:20,
        paddingLeft:15,
        paddingTop:8,
        paddingBottom:8,
        marginTop:0,
        marginRight:10,
        borderWidth:2,
        borderColor:'#665',
        borderBottomLeftRadius:50,
        borderTopLeftRadius:50,
        borderBottomRightRadius:50,
        borderTopRightRadius:50,
    },


}) 