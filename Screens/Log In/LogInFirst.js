import React from 'react';
import { bindActionCreators } from 'redux';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
  ToastAndroid

} from 'react-native';
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import SmsListener from 'react-native-android-sms-listener'
import PushNotification from 'react-native-push-notification';
import { connect } from 'react-redux'
import * as RegistrationActions from '../../actions/registrationActions';
import { generateSecureRandom } from 'react-native-securerandom';
import base64 from 'base-64';

//http://rafshadow.ir/GetRegistrationStatus?phone=09122139294&serviceId=608


function mapStateToProps({ registrationInfo,userInfo }) {
  return {
    phoneNumber: registrationInfo.phoneNumber,
    isRegistered: registrationInfo.isRegistered,
    weekNumber:userInfo.weekNumber
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(RegistrationActions, dispatch)
}

@connect(mapStateToProps, mapDispatchToProps)
export default class LogInFirst extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: this.props.phoneNumber,
    };

    this._handleValidation = this._handleValidation.bind(this);
    this._handleSendAndReceivePushNotification = this._handleSendAndReceivePushNotification.bind(this);
    this.sendNotificationRegistrationSuccess = this.sendNotificationRegistrationSuccess.bind(this)
  }

  componentDidMount(){
    this.props.checkIsRegistered(this.props.isRegistered,this.props.phoneNumber)
    const {isRegistered,weekNumber} = this.props
    if(isRegistered && weekNumber===0){
      this.props.navigation.replace('LogInThird')
    } else if (isRegistered && weekNumber!==0) {
      this.props.navigation.replace('Home')
    }
    this.passwordGenerator()
  }


  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      headerStyle: { height: 35 },
      drawerLockMode: 'locked-closed',
      headerLeft:null,
    }
  };

  // Password Generator For Sending To Server
  passwordGenerator(){
    generateSecureRandom(12).then(randomBytes => {
      var password = base64.encode(randomBytes);
      console.log('Password is : ', password)
    });
  }

  // Dispatch Functions
  setPhoneNumber(phoneNumber) {
    this.props.setPhoneNumber(phoneNumber);
  }

  setRegistrationComplete() {
    this.props.setRegistrationComplete();
  }

  sendNotificationRegistrationSuccess(){
    this.props.sendNotificationRegistrationSuccess();
    this.customPushNotification("ثبت نام شما با موفقیت انجام شد.")
  }


  
  customPushNotification(message){
    PushNotification.localNotification({
      autoCancel: true,
      message:message,
      color: "red", 
      ongoing: false,
      vibrate: true, 
      vibration: 300, 
      actions:null
    });
  }
  
  fetchPhoneNumber(phoneNumber) {
    fetch(`http://rafshadow.ir/SendOperatorOtpRequest?phone=${phoneNumber}&serviceId=608`)
      .then((response) => response.json())
      .then((res) => {
        console.log('Delivered PhoneNumber From RafShadow : ', res);
        if (res === `the phone: ${phoneNumber} status is registered`) {
          this.setRegistrationComplete();
          this.customPushNotification("شما قبلا ثبت نام کرده اید.")
        }
      });
  }


  _handleSendAndReceivePushNotification(phoneNumber) {
    const that = this;
    PushNotification.configure({
      foreground: false,
      userInteraction: false,
      message: 'My Notification Message',
      data: {}, 
      onRegister: function (token) {
        if (phoneNumber !== null) {
          let json = {
            token: token.token,
            phoneNumber: phoneNumber
          };
          fetch('http://rafshadow.ir/notification', {
            method: 'POST',
            headers: {
              'content-type': 'application/json'
            },
            body: JSON.stringify(json)
          })
          .then((response) => response.json())
          .then((res) => {
            console.log('Delivered Token And PhonNumber From RafShadow :', res);
          });
        }
      },
      onNotification: function ({ notification }) {
        console.log('NOTIFICATION:', notification);
        if(notification){
          if (notification.body === 'Registration done successfully with the Telco.') {
            that.sendNotificationRegistrationSuccess();
          }
        }
      },
      senderID: '713429133284',
      popInitialNotification: true,
      requestPermissions: true,

    });
  }

  _handleValidation() {
    const {phoneNumber} = this.state
    
    if (phoneNumber !== null) {
      if (phoneNumber.slice(0, 1) !== '0' && phoneNumber !== '' || phoneNumber.length!==11) {
        alert('شماره تلفن وارد شده صحیح نیست');
        return false
      } else {
        return true
      }
    } else {
      return true
    }
  }

  
//......................Handle Responsive Style......................

 
  _handleHeaderFontSize(){
    if(height(100)<600){
        return(16)
    }else{
        return(20)
    }
  }


  _handleSubHeaderFontSize(){
    if(height(100)<600){
        return(14)
    }else{
        return(16)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(70)
    }else{
        return(100)
    }
  }


  _handleButtonTextFontSize(){
    if(height(100)<600){
      return(13)
    }else{
        return(16)
    }
  }

  _handleEnterTextFontSize(){
    if(height(100)<600){
      return(10)
    }else{
        return(12)
    }
  }

  render() {
    const { phoneNumber,registeredBefore } = this.state;
    const { setPhoneNumber, setRegistrationComplete } = this.props;
    return (
      <ScrollView style={[styles.container]}>
        <View style={[styles.header]}>
          <Image
            style={[styles.headerImage]}
            source={require('../../Assets/Images/Login1.jpg')}
          />
          <Text style={[styles.headerText,{
            fontSize:this._handleHeaderFontSize()
          }]}>به کالسکه خوش آمدید</Text>
          <Text style={[styles.subHeaderText,{
            fontSize:this._handleSubHeaderFontSize()
          }]}>برای فعال شدن برنامه، شماره تلفن همراه خود را وارد کنید. یک کد چهار رقمی
            برای شما ارسال میشود؛ پس از دریافت کد آن را در مستطیل زیر وارد کنید.
          </Text>
        </View>
        <FormInput
          underlineColorAndroid={'#bcbcbc'}
          maxLength={11}
          //onSelectionChange={}
          onChangeText={(phoneNumber) => {
            this.setState({ phoneNumber });
          }}
          keyboardType='numeric'
          inputStyle={{ textAlign: 'center' }}
          placeholder={'شماره تلفن همراه خود را وارد کنید'}
          value={this.state.phoneNumber}/>

        <View style={{ marginTop: 10, alignSelf: 'center' }}>
          <TouchableOpacity
            onPress={() => {
              this._handleValidation()
              setPhoneNumber(phoneNumber);
              this._handleSendAndReceivePushNotification(phoneNumber);
              this.fetchPhoneNumber(phoneNumber);
              if(this._handleValidation()){
                this.props.navigation.replace('LogInSecond');                
              }
            }}
            style={[styles.button,{
              width: this._handleButtonWidth(),  
            }]}
          >
            <Text style={[styles.buttonText,{
              fontSize: this._handleButtonTextFontSize(),                              
            }]}>ارسال</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              console.log(this.props.navigation);
              this.props.navigation.replace('Home')
            }}
            style={[styles.buttonEnter, {width:300}]}
          >
            <Text
              style={[styles.buttonText, {
                fontSize: this._handleEnterTextFontSize(),
                color: 'blue',
                borderBottomWidth: 1
              }]}
            >
              ورود به نسخه آزمایشی
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.Footer]}>
        </View>
      </ScrollView>


    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    alignItems: 'center',
    marginTop: 0,
  },
  headerImage: {
    width: width(100),
    height: height(30)

  },
  headerText: {
    marginTop: 10,
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',
    alignSelf: 'center',

  },
  subHeaderText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,

  },
  button: {
    backgroundColor: '#f4b642',
    height:50,
    marginTop: 5,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'

  },
  buttonEnter: {
    backgroundColor: '#fff',
    width: 110,
    height: 60,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'

  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',

  },
  Footer: {
    height: 20,
  },


});
