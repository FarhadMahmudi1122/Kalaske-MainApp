import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
  Keyboard,
  TextInput,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-picker';
import Modal from 'react-native-modal';
import JalaliCalendarPicker from 'react-native-jalali-calendar-picker';
import { connect } from 'react-redux'
import * as userInputActions from '../../actions/userActions'

function mapStateToProps({ userInfo }) {
  //console.log(userInfo);
  return {
    userInfo
  }
}

@connect(mapStateToProps)
export default class LogInFourth extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...this.props.userInfo };

    this._handelHeightPicker = this._handelHeightPicker.bind(this);
    this._handelWeightPicker = this._handelWeightPicker.bind(this);
    this._handelDatePicker = this._handelDatePicker.bind(this);
    this._handelImagePicker = this._handelImagePicker.bind(this);
    this._handleProfileSourceImage = this._handleProfileSourceImage.bind(this);
    this._handleProfileImageStyle = this._handleProfileImageStyle.bind(this);
  }

  static navigationOptions = ({ navigation, screenProps }) => {
    const params = navigation.state.params || {};
    return {
      headerStyle: { height: 35 },
      drawerLockMode: 'locked-closed',
      headerRight: <Text style={[styles.topHeaderRight,{
        fontSize:params._handleTopHeaderRightFontSize
      }]}> پروفایل مادر و کودک </Text>,
      headerLeft:null
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({ _handleTopHeaderRightFontSize: this._handleTopHeaderRightFontSize() });
  }


  _handelWeightPicker() {
    weightData = [];
    for (var i = 40; i < 150; i++) {
      weightData.push(i);
    }

    const setWeight = (weightNumber) => {
      this.setState({ motherWeight: weightNumber })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: weightData,
      selectedValue: [this.state.motherWeight],
      onPickerConfirm: data => {
        setWeight(parseInt(data[0], 10))
      }
    });
    Picker.show();
  };

  _handelHeightPicker() {
    heightData = [];
    for (var i = 140; i < 200; i++) {
      heightData.push(i);
    }

    const setHeight = (motherHeight) => {
      this.setState({ motherHeight })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: heightData,
      selectedValue: [this.state.motherHeight],
      onPickerConfirm: data => {
        setHeight(parseInt(data[0], 10))
      }
    });
    Picker.show();
  }

  _handelImagePicker() {
    let options = {
      title: 'عکس خود را انتخاب کنید',
      takePhotoButtonTitle: 'عکس با دوربین',
      chooseFromLibraryButtonTitle: 'انتخاب از گالری',
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let avatarSource = { uri: response.uri };
        this.props.dispatch(userInputActions.setAvatarSource(avatarSource))
      }
    });

  }

  _handelDatePicker() {
    DateData = pickerData = [
      [1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376],
      ['مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند', 'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور'],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
    ];


    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: DateData,
      selectedValue: [this.state.motherYear, this.state.motherMonth, this.state.motherDay],
      onPickerConfirm: data => {

        this.setState({
          motherYear: data[0],
          motherMonth: data[1],
          motherDay: data[2],
        })
      }
    });
    Picker.show();
  }

  _handleProfileSourceImage() {
    if (this.state.avatarSource !== null) {
      return this.state.avatarSource
    } else {
      return require('../../Assets/Images/Profileicon.png');
    }

  }

  _handleProfileImageStyle() {
    if (this.state.avatarSource) {
      return (styles.profileImagePicked,{
        width:_handleImageProfilePickedHeightAndWidth(),//Handle Responsive Style
        height:_handleImageProfilePickedHeightAndWidth()//Handle Responsive Style
      });

    } else {
      return (styles.profileImage,{
        width:this._handleImageProfileHeightAndWidth(),//Handle Responsive Style
        height:this._handleImageProfileHeightAndWidth()//Handle Responsive Style
      });
    }

  }


  _handleNameValue(motherName) {
    this.setState({ motherName })
  }

  _handleEmailValue(inputValue) {
    this.setState({
      motherEmail: inputValue,
    })
  }

  _handleDipatchInfo() {
    const {
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherEmail,
      motherName,
      motherHeight,
      motherWeight,
    } = this.state;

    this.props.dispatch(userInputActions.setUserInfoFromFourthLogin(
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherEmail,
      motherName,
      motherHeight,
      motherWeight,
    ));

  }

//......................Handle Responsive Style......................


  _handleTopHeaderRightFontSize(){
    if(height(100)<600){
      return(16)
    }else{
      return(20)
    }
  }

  _handleHeaderFontSize(){
    if(height(100)<600){
        return(14)
    }else{
        return(18)
    }
  }

  _handleSubHeaderFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  _handleDescriptionFontSize(){
    if(height(100)<600){
        return(10)
    }else{
        return(12)
    }
  }

  _handleImageProfileHeightAndWidth(){
    if(height(100)<600){
      return(70)
    }else{
      return(100)
    }
  }
  
  _handleImageProfilePickedHeightAndWidth(){
    if(height(100)<600){
      return(20)
    }else{
      return(30)
    }
  }

  _handleCameraIconSize(){
    if(height(100)<600){
      return(20)
    }else{
      return(30)
    }
  }
  _handleInputLableFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  _handleInputlableMarginBottom(){
    if(height(100)<600){
      return(-height(4))
    }else{
      return(-height(2))
    }
  }

  _handleInputFontSize(){
    if(height(100)<600){
        return(11)
    }else{
        return(null)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(70)
    }else{
        return(100)
    }
  }

  _handleButtonTextFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  render() {
    const {
      motherDay,
      motherMonth,
      motherYear,
      avatarSource,
      motherEmail,
      motherName,
      motherHeight,
      motherWeight
    } = this.state;

    return (
      <ScrollView style={[styles.container]}>
        <View style={[styles.header]}>
          <Text style={[styles.headerText,{
            fontSize:this._handleHeaderFontSize()
          }]}>مرحله اول</Text>
          <Text style={[styles.subHeaderText, {
              fontSize: this._handleSubHeaderFontSize(),
              marginBottom: 2 
             }]}>مشخصات مادر</Text>
          <Text style={[styles.subHeaderText,{
            fontSize: this._handleDescriptionFontSize(),
          }]}>
            به ما کمک کنید تا با داشتن مشخصات فیزیکی شما مطالب مرتبط با شرایطتان را در اختیارتان بگذاریم.
          </Text>
        </View>
        <View>
          <View style={[styles.profileImage, { borderRadius: 50, overflow: 'hidden' }]}>
            <Image
              resizeMode='contain'
              style={this._handleProfileImageStyle()}
              source={this._handleProfileSourceImage()}
            />
          </View>
          <TouchableOpacity
            style={[styles.profileImageBtn]}
            onPress={this._handelImagePicker}
          >
            <MaterialIcons
              style={{ alignSelf: 'center', }}
              name={'photo-camera'}
              size={this._handleCameraIconSize()}
            />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: -height(5) }}>
          <View>
            <Text style={[styles.inputLable,{
              fontSize: this._handleInputLableFontSize(),
              marginBottom: this._handleInputlableMarginBottom()              
            }]}>نام</Text>
            <TextInput
              onChangeText={(value) => this._handleNameValue(value)}
              underlineColorAndroid={'#bcbcbc'}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize(),
              }]}
              placeholder={'نام خود را وارد کنید'}
              value={`${motherName}`}/>
          </View>
          <View>
            <Text style={[styles.inputLable,{
              fontSize: this._handleInputLableFontSize(),
              marginBottom: this._handleInputlableMarginBottom()              
            }]}>ایمیل</Text>
            <TextInput
              onChangeText={(value) => this._handleEmailValue(value)}
              underlineColorAndroid={'#bcbcbc'}
              keyboardType='email-address'
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize(),
              }]}
              placeholder={'ایمیل خود را وارد کنید'}
              value={`${motherEmail}`}/>
          </View>
          <TouchableOpacity onPress={this._handelDatePicker}>
            <Text style={[styles.inputLable,{
              fontSize: this._handleInputLableFontSize(),
              marginBottom: this._handleInputlableMarginBottom() 
            }]}>تاریخ تولد مادر</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize(),
              }]}
              placeholder={`روز ماه سال`}
              value={`${motherDay} ${motherMonth} ${motherYear}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelWeightPicker}>
            <Text style={[styles.inputLable,{
              fontSize: this._handleInputLableFontSize(),
              marginBottom: this._handleInputlableMarginBottom() 
            }]}>وزن پیش از زایمان</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize(),
              }]}
              placeholder={'وزن'}
              value={`${motherWeight}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelHeightPicker}>
            <Text style={[styles.inputLable,{
              fontSize: this._handleInputLableFontSize(),
              marginBottom: this._handleInputlableMarginBottom() 
            }]}>قد</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              placeholderTextColor={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={`${motherHeight} سانتیمتر`}
              value={`${motherHeight} سانتیمتر`}/>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            this._handleDipatchInfo();
            this.props.navigation.replace('LogInFifth');
          }}
          style={[styles.button,{
            width:this._handleButtonWidth()
          }]}
        >
          <Text style={[styles.buttonText,{
            fontSize:this._handleButtonTextFontSize()
          }]}>مرحله بعد</Text>
        </TouchableOpacity>
        <View style={[styles.Footer]}>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({

  topHeaderRight: {
    padding: 10,
    fontWeight: 'bold',
    color: '#000',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  calendarModal: {
    marginTop: height(14),
    marginLeft: width(1),
    marginRight: width(1),
    marginBottom: height(14)
  },
  modalContent: {
    margin: 10,
    height: 350,
    flex: 1,
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  header: {
    alignItems: 'center',
    marginTop: 5,
  },
  headerText: {
    marginBottom: 5,
    marginTop: 5,
    marginRight: 20,
    paddingRight: 10,
    borderRightWidth: 3,
    borderRightColor: '#f4b642',
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',

  },
  subHeaderText: {
    color: 'gray',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'right',
    alignSelf: 'flex-end',
    marginLeft: 20,
    marginRight: 20,
    marginTop: -10,
    marginBottom: 8
  },
  profileImage: {
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: '#ccc',
  },
  profileImagePicked: {
    width: width(40),
    height: 120,
    alignSelf: 'center',
    top: -10
  },
  profileImageBtn: {
    width: 30,
    height: 30,
    borderRadius: 15,
    alignSelf: 'center',
    top: -20,
    left: 30,
    alignItems: 'center',
    zIndex: 100

  },
  inputLable: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',
    paddingRight: 18,
  },
  textInputStyle: {
    textAlign: 'right',
    width: width(95),
    alignSelf: 'center',
  },
  touchableInputLable: {
    borderBottomWidth: 2,
    borderColor: 'gray'
  },
  button: {
    backgroundColor: '#f4b642',
    width: 100,
    height: 50,
    marginLeft: 20,
    marginTop: 5,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'

  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    fontSize: 16,
    textAlign: 'center',

  },
  Footer: {
    height: 20,
  },
});
