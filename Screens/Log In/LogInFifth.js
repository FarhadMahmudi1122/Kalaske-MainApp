import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
  Keyboard,
  TextInput
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import Picker from 'react-native-picker';
import { connect } from 'react-redux';
import * as userInputActions from '../../actions/userActions'

function mapStateToProps({ userInfo }) {
  return {
    babyInfo: {
      babyNumber: userInfo.babyNumber,
      babyName: userInfo.babyName,
      babySex: userInfo.babySex
    }
  }
}

@connect(mapStateToProps)
export default class LogInFifth extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.babyInfo);
    this.state = { ...this.props.babyInfo };

    this._handelBabyNumberPicker = this._handelBabyNumberPicker.bind(this);
    this._handelBabySexPicker = this._handelBabySexPicker.bind(this);

  }

  static navigationOptions = ({ navigation, screenProps }) => {
    const params = navigation.state.params || {};    
    return {
      headerStyle: { height: 35 },
      drawerLockMode: 'locked-closed',
      headerRight: <Text style={[styles.topHeaderRight,{
        fontSize:params._handleTopHeaderRightFontSize
      }]}> پروفایل مادر و کودک </Text>,
      headerLeft:null
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({ _handleTopHeaderRightFontSize: this._handleTopHeaderRightFontSize() });
  }


  _handelBabyNumberPicker() {
    const babyNumberData = [];
    for (var i = 0; i < 8; i++) {
      babyNumberData.push(i);
    }

    //console.log(this);
    const setBabyNumber = (babyNumber) => {
      this.setState({ babyNumber })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: babyNumberData,
      selectedValue: [this.state.babyNumber],
      onPickerConfirm: data => {
        //console.log(data);
        setBabyNumber(parseInt(data[0], 10))
      }
    });
    Picker.show();
  }

  _handelBabySexPicker() {
    const babySexData = ['دختر', 'پسر'];

    const setBabySex = (babySex) => {
      this.setState({ babySex })
    };

    Picker.init({
      pickerTitleText: 'انتخاب کنید',
      pickerConfirmBtnText: 'تائید',
      pickerCancelBtnText: 'انصراف',
      pickerData: babySexData,
      selectedValue: [this.state.babySex],
      onPickerConfirm: data => {
        setBabySex(data[0])
      }
    });
    Picker.show();
  }

  _handleDipatchInfo() {
    const {
      babyName,
      babySex,
      babyNumber
    } = this.state;

    this.props.dispatch(userInputActions.setBabyInfoFromFifthLogin(babyName, babySex, babyNumber));

  }


  //......................Handle Responsive Style......................


  _handleTopHeaderRightFontSize(){
    if(height(100)<600){
      return(16)
    }else{
      return(20)
    }
  }

  _handleHeaderFontSize(){
    if(height(100)<600){
        return(14)
    }else{
        return(18)
    }
  }

  _handleSubHeaderFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  _handleDescriptionFontSize(){
    if(height(100)<600){
        return(10)
    }else{
        return(12)
    }
  }


  _handleInputLableFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(14)
    }
  }

  _handleInputFontSize(){
    if(height(100)<600){
        return(11)
    }else{
        return(null)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(80)
    }else{
        return(110)
    }
  }

  _handleButtonTextFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={[styles.header]}>
          <Text style={[styles.headerText,{
            fontSize:this._handleHeaderFontSize()
          }]}>مرحله دوم</Text>
          <Text style={[styles.subHeaderText, { 
            fontSize: this._handleSubHeaderFontSize(),
             marginBottom: 2 
             }]}>مشخصات کودک</Text>
          <Text style={[styles.subHeaderText,{
            fontSize:this._handleDescriptionFontSize()
          }]}>
            از کودکتان چه میدانید؟ با ما درمیان بگذارید
          </Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <View>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>نام انتخابی کودک</Text>
            <TextInput
              underlineColorAndroid={'#bcbcbc'}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              onChangeText={(value) => this.setState({ babyName: value })}
              placeholder={'برای فرزند خود چه نامی انتخاب کرده اید؟'}
              value={`${this.state.babyName}`}
            />
          </View>
          <TouchableOpacity onPress={this._handelBabySexPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>جنسیت کودک</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={'دختر یا پسر؟'}
              value={`${this.state.babySex}`}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._handelBabyNumberPicker}>
            <Text style={[styles.inputLable,{
              fontSize:this._handleInputLableFontSize()
            }]}>فرزند چندم شماست</Text>
            <TextInput
              underlineColorAndroid={'#000'}
              editable={false}
              style={[styles.textInputStyle,{
                fontSize:this._handleInputFontSize()
              }]}
              placeholder={`${this.state.babyNumber}`}
              value={`${this.state.babyNumber}`}/>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
          <TouchableOpacity
            onPress={() => {
              this._handleDipatchInfo();
              this.props.navigation.replace('Home');
            }}
            style={[styles.button,{
              width:this._handleButtonWidth()
            }]}
          >
            <Text style={[styles.buttonText,{
              fontSize:this._handleButtonTextFontSize()
            }]}>ذخیره تغییرات</Text>
          </TouchableOpacity>

        </View>
        <View style={[styles.Footer]}>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  topHeaderRight: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 20,
    color: '#000',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    alignItems: 'center',
    marginTop: 5,
  },
  headerText: {
    marginBottom: 5,
    marginTop: 5,
    marginRight: 20,
    paddingRight: 10,
    borderRightWidth: 3,
    borderRightColor: '#f4b642',
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',

  },
  subHeaderText: {
    color: 'gray',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'right',
    alignSelf: 'flex-end',
    marginLeft: 20,
    marginRight: 20,
    marginTop: -10,
    marginBottom: 20
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 500,
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#ccc'
  },
  profileImageBtn: {
    width: 30,
    height: 30,
    borderRadius: 15,
    alignSelf: 'center',
    top: -20,
    left: 30,
    alignItems: 'center',


  },
  inputLable: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    alignSelf: 'flex-end',
    paddingRight: 18,
    marginBottom: -15
  },
  textInputStyle: {
    textAlign: 'right',
    width: width(95),
    alignSelf: 'center',
  },
  touchableInputLable: {
    borderBottomWidth: 2,
    borderColor: 'gray'
  },
  touchableInputLableText: {},
  button: {
    backgroundColor: '#f4b642',
    height: 50,
    marginTop: 20,
    borderRadius: 15,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',

  },
  enterbutton: {
    backgroundColor: '#f4b642',
    width: 150,
    height: 60,
    marginLeft: 50,
    marginTop: 20,
    borderRadius: 20,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Footer: {
    height: 20,
  },


});
