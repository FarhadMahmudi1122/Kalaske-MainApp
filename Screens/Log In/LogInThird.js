import React from 'react';
import { bindActionCreators } from 'redux';
import {
    Text,
    View,
    Image,
    StyleSheet, 
    ScrollView,
    Dimensions,
    TouchableNativeFeedback,
    Platform,
    TouchableOpacity,
    Animated,
    Easing,

} from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FormLabel, FormInput, Card, Button } from 'react-native-elements';
import { width, height, totalSize } from 'react-native-dimension';
import Modal from "react-native-modal";
import JalaliCalendarPicker from 'react-native-jalali-calendar-picker';
import Picker from 'react-native-picker';
import moment from 'moment-jalaali';
import { connect } from 'react-redux'
import * as UserActions from '../../actions/userActions'
import * as RegistrationActions from '../../actions/registrationActions';


function mapStateToProps({ userInfo ,registrationInfo }) {
    return {
      weekNumber: userInfo.weekNumber,
      isRegistered: registrationInfo.isRegistered,
      phoneNumber: registrationInfo.phoneNumber
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        ...UserActions,
        ...RegistrationActions
    }, dispatch)
}

@connect(mapStateToProps,mapDispatchToProps)
export default class LogInThird extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            weekNumber:0,
            selectedStartDateForBirth: null,
            selectedStartDateForGhaedegi:null,
            visibleModal: null,
            DropDownBtn:new Animated.Value(0),
            arrowDropDownDisplay:'flex',
            arrowDropUpDisplay:'none',
            dropDownBtnViewDisplay:'none',
            definedValueForWeekNumber:null,
            disabledForWeekNumber:null,
            definedValueForBirth:null,
            disabledForBirth:null,
            definedDateForGhaedegi:null,
            disabledForGhaedegi:null
        };
        this.onDateChangeForBirth = this.onDateChangeForBirth.bind(this);
        this.onDateChangeForGhaedegi = this.onDateChangeForGhaedegi.bind(this);
        this._handlePicker = this._handlePicker.bind(this);
        this._handleSelectedStartDateForBirth = this._handleSelectedStartDateForBirth.bind(this);
        this._handleSelectedStartDateForGhaedegi = this._handleSelectedStartDateForGhaedegi.bind(this)
    }


    componentDidMount(){
        this.props.checkIsRegistered(this.props.isRegistered,this.props.phoneNumber)
        if(this.props.weekNumber!==0){
            this.props.navigation.replace('Home')
        }
    }


    static navigationOptions = ({ navigation, screenProps }) => {
        return {
            headerStyle: {height: 35},
            drawerLockMode:'locked-closed',
            headerLeft:null
        }
    };

    // Dispatch Functions
    setDaysTillChildbirthThirdLogin(
        childbirthDays,
        childbirthDateFormatted,
        weekNumber,
        
    ){
        this.props.setDaysTillChildbirthThirdLogin(
            childbirthDays,
            childbirthDateFormatted,
            weekNumber,
            
        )
    }


    _handlePicker(){
        weekNumberdata = [];
        for(var i=1;i<42;i++){
            weekNumberdata.push(i);
        }

        const setWeek = (weekNumber) => {
            this.setState({weekNumber: weekNumber})
        }
    
        Picker.init({
            pickerTitleText:'انتخاب کنید',
            pickerConfirmBtnText:'تائید',
            pickerCancelBtnText:'انصراف',
            pickerData: weekNumberdata,
            selectedValue: [5],
            onPickerConfirm: data => {
                setWeek(Number(data[0]));
                const { weekNumber } = this.state;

                //date calculating for week button and dispatch

                let childbirthDateFormatted = moment(
                    ((40-weekNumber)* 7 * 1000 * 60 * 60 * 24) + 
                    moment().valueOf()).format('jYYYY/jM/jD');
                let ghaedegiDateFromWeekNumber = moment(
                    (moment().valueOf()-(weekNumber-1) * 7 * 1000 * 60 * 60 * 24)).format('jYYYY/jM/jD');
                
                //date calculating for dispatch

                let childbirthDays = (40-weekNumber)* 7

                // set state for a date chosen with week button

                this.setState({
                definedValueForBirth : childbirthDateFormatted,
                disabledForBirth:true,
                definedDateForGhaedegi : ghaedegiDateFromWeekNumber,
                disabledForGhaedegi:true
                });

                //dispatch date to motherProfilescreen 

                this.setDaysTillChildbirthThirdLogin(
                    childbirthDays,
                    childbirthDateFormatted,
                    weekNumber,
                     
                );

            }
        });
        Picker.show();
      }

    onDateChangeForBirth(dateForBirth) {
        this.setState({
          selectedStartDateForBirth: dateForBirth,
        });
    }

    onDateChangeForGhaedegi(dateForGhaedegi) {
        this.setState({
          selectedStartDateForGhaedegi: dateForGhaedegi,
        });
    }

    _handleSelectedStartDateForBirth(){
        const {selectedStartDateForBirth} = this.state;
        
        if (selectedStartDateForBirth === null){
            this.setState({ 
                visibleModal: null ,
            })
        } else {
            //Date Calculating for childbirth button

            let weekNumber = 40-(Math.ceil(Math.ceil(
                (selectedStartDateForBirth.valueOf()-moment().valueOf()) /
                (1000 * 60 * 60 * 24))/7)-1)
            let dateForGhaedegi = moment(
                selectedStartDateForBirth.valueOf() -
                (280 * 1000 * 60 * 60 * 24)).format('jYYYY/jM/jD');
            
            //Date Calculating for dispatch

            let selectedDateForChildbirth = selectedStartDateForBirth.format('jYYYY/jM/jD')
            let childbirthDays = (Math.ceil(Math.ceil(
                (selectedStartDateForBirth.valueOf()-moment().valueOf()) /
                (1000 * 60 * 60 * 24))))            
            
            //set state for a date chosen with childbirth button

            this.setState({ 
                visibleModal: null ,
                definedValueForWeekNumber : weekNumber,
                disabledForWeekNumber:true,
                definedDateForGhaedegi : dateForGhaedegi,
                disabledForGhaedegi:true
            })

            //disatch date to motherProfileScreen

            this.setDaysTillChildbirthThirdLogin(
                childbirthDays,
                selectedDateForChildbirth,
                weekNumber,
                 
            );
            
        }
    }
    _handleSelectedStartDateForGhaedegi (){
        const { selectedStartDateForGhaedegi } = this.state;

        if(selectedStartDateForGhaedegi === null){
            this.setState({ 
                visibleModal: null ,
            })
        } else {
            //Date Calculating for childbirth button

            let slelectedDateForChildbirth = moment(
                selectedStartDateForGhaedegi.valueOf() +
                (280 * 1000 * 60 * 60 * 24)).format('jYYYY/jM/jD')
            let weekNumber = Math.ceil(Math.ceil(
                (moment().valueOf()-selectedStartDateForGhaedegi.valueOf()) /
                (1000 * 60 * 60 * 24))/7)
            //Date Calculating for dispatch

            let childbirthDays = (40-weekNumber)* 7

            //set state for a date chosen with childbirth button

            this.setState({ 
                visibleModal: null,
                definedValueForBirth : slelectedDateForChildbirth,
                disabledForBirth:true,
                definedValueForWeekNumber : weekNumber,
                disabledForWeekNumber:true,
                })

            //disatch date to motherProfileScreen

            this.setDaysTillChildbirthThirdLogin(
                childbirthDays,
                slelectedDateForChildbirth,
                weekNumber,
                 
            );
        }
    }
      

//......................Handle Responsive Style......................

 
    _handleHeaderAndSubButtonsFontSize(){
        if(height(100)<600){
        return(13)
        }else{
            return(18)
        }
    }

    _handleButtonWidth(){
        if(height(100)<600){
          return(100)
        }else{
            return(150)
        }
    }
    
    
    _handleButtonTextFontSize(){
        if(height(100)<600){
            return(12)
        }else{
            return(16)
        }
    }

    _handleEnterTextFontSize(){
        if(height(100)<600){
            return(10)
        }else{
            return(12)
        } 
    }
    
    _handleEnterTextHeight(){
        if(height(100)<600){
            return(30)
        }else{
            return(60)
        } 
    }

    _handleButtonMarginTop(){
        if(height(100)<600){
            return(0)
        }else{
            return(10)
        } 
    }

    render(){
        const { 
            test,
            weekNumber,
            selectedStartDateForBirth,
            selectedStartDateForGhaedegi,
            visibleModal,
            DropDownBtn,
            arrowDropDownDisplay,
            arrowDropUpDisplay,
            dropDownBtnViewDisplay,
            definedValueForWeekNumber,
            disabledForWeekNumber,
            definedValueForBirth,
            disabledForBirth,
            definedDateForGhaedegi,
            disabledForGhaedegi
        } = this.state;

        const startDateForBirth = selectedStartDateForBirth ? 
        selectedStartDateForBirth.format('jYYYY/jM/jD') : '';
        const startDateForGhaedegi = selectedStartDateForGhaedegi ? 
        selectedStartDateForGhaedegi.format('jYYYY/jM/jD') : '';
        const weekNumberIfNotZiro = weekNumber === 0 ? null :weekNumber

        return (
            <View style={[styles.container]}>
                <Modal
                    style={styles.calendarModal}
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={this.state.visibleModal === 2}
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={800}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={800}
                    backdropTransitionOutTiming={800}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                        <JalaliCalendarPicker
                            minDate={new Date((new Date()).valueOf() + 1000*3600*24)}
                            onDateChange={this.onDateChangeForBirth}
                            width={width(100)}
                            height={height(55)}
                        />
                        <Text style={{fontSize:16,fontWeight:'bold'}}>تاریخ انتخابی شما:  { startDateForBirth }</Text>
                        <TouchableOpacity onPress={this._handleSelectedStartDateForBirth}>
                            <View style={[styles.button,{width:100,height:50}]}>
                                <Text style={{fontSize:20,fontWeight:'bold'}}>تائید</Text>
                            </View>
                        </TouchableOpacity> 
                    </View>
                </Modal>
                <Modal
                    style={styles.calendarModal}
                    onBackdropPress={() => this.setState({ visibleModal: false })}
                    isVisible={this.state.visibleModal === 3}
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={800}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={800}
                    backdropTransitionOutTiming={800}
                    useNativeDriver={true}
                >
                    <View style={styles.modalContent}>
                        <JalaliCalendarPicker
                            maxDate={new Date((new Date()).valueOf() - 1000*3600*24)}
                            onDateChange={this.onDateChangeForGhaedegi}
                            width={width(100)}
                            height={height(55)}
                        />
                        <Text style={{fontSize:16,fontWeight:'bold'}}>تاریخ انتخابی شما:  { startDateForGhaedegi }</Text>
                        <TouchableOpacity onPress={this._handleSelectedStartDateForGhaedegi}>
                            <View style={[styles.button,{width:100,height:50}]}>
                                <Text style={{fontSize:20,fontWeight:'bold'}}>تائید</Text>
                            </View>
                        </TouchableOpacity> 
                    </View>
                </Modal>

                <View style={[styles.header]}>
                    <Image
                        resizeMode={'contain'} 
                        style={[styles.headerImage]} 
                        source={require('../../Assets/Images/Login3.jpg')}
                    />
                    <View
                        style={{flexDirection:'row',alignSelf:'flex-end'}}
                    >
                        <Text style={[styles.headerText,{
                            fontSize:this._handleHeaderAndSubButtonsFontSize()
                        }]}>تعیین تاریخ زایمان</Text>
                    </View>
                </View>
                <Text style={{marginRight:20,marginTop:10,color:'#f4b642'}}>یکی از گزینه های زیر را انتخاب کنید</Text>
                <View style={[styles.dropDownBtnView]}>
                    <TouchableOpacity 
                        disabled={disabledForWeekNumber}
                        onPress={this._handlePicker}
                        style={[styles.dropDownBtn]}
                    >
                        <Text style={[styles.dropDownBtnText,{
                            fontSize:this._handleHeaderAndSubButtonsFontSize()
                        }]}>هفته چندم           {weekNumberIfNotZiro}{definedValueForWeekNumber}</Text>
                    </TouchableOpacity> 
                    <TouchableOpacity 
                        disabled={disabledForBirth}
                        onPress={() => this.setState({
                            visibleModal: 2,
                        })}
                        style={[styles.dropDownBtn]}
                    >
                        <Text style={[styles.dropDownBtnText,{
                            fontSize:this._handleHeaderAndSubButtonsFontSize()
                        }]}>تاریخ زایمان         {startDateForBirth}{definedValueForBirth}</Text>
                    </TouchableOpacity> 
                    <TouchableOpacity 
                        disabled={disabledForGhaedegi}
                        onPress={() => this.setState({
                            visibleModal: 3,
                        })}
                        style={[styles.dropDownBtn]}
                    >
                        <Text style={[styles.dropDownBtnText,{
                            fontSize:this._handleHeaderAndSubButtonsFontSize()
                        }]}>آخرین قاعدگی      {startDateForGhaedegi}{definedDateForGhaedegi}</Text>
                    </TouchableOpacity> 
                </View>
                <View style={{marginTop:20,alignSelf:"center"}}>
                    <TouchableOpacity 
                        onPress={() => {
                            this.props.navigation.replace('LogInFourth');
                            //this._handleDispatch()
                            }}
                        style={[styles.button,{
                            width:this._handleButtonWidth(),
                            marginTop:this._handleButtonMarginTop()

                        }]}
                    >
                        <Text style={[styles.buttonText,{
                            fontSize:this._handleButtonTextFontSize()
                        }]}>تکمیل مشخصات</Text>
                    </TouchableOpacity> 
                    <TouchableOpacity 
                        onPress={() => this.props.navigation.replace('Home')}
                        style={[styles.buttonEnter,{
                            width:300,
                            height:this._handleEnterTextHeight()
                            }]}
                    >
                        <Text style={[styles.buttonText,{
                            fontSize:this._handleEnterTextFontSize(),
                            color:'blue',
                            borderBottomWidth:1
                            }]}>ورود به نسخه آزمایشی</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.Footer]}>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white'
    },
    modalContent: {
        margin:10,
        height:350,
        flex:1,
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    calendarModal:{
        marginTop: height(14),
        marginLeft:width(1),
        marginRight:width(1),
        marginBottom:height(14)
    },
    bottomChoseWeekModal: {
        alignItems:'center',
        marginTop: height(34),
        marginLeft:width(15),
        marginRight:width(15),
        marginBottom:height(30)
    },
    header:{
        alignItems:'center',
        marginTop:0,
    },
    headerImage:{
        
        width:width(100),
        height:height(30)
        
    },
    headerText:{
        marginTop:10,
        marginRight:10,
        paddingRight:10,
        borderRightWidth:3,
        borderRightColor:'#ccc',
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:20,
        alignSelf:'flex-end',
        
    },
    dropDownBtnView:{
        borderWidth:0,
        marginTop:10,
    },
    dropDownBtn:{
        marginTop:10,        
        borderRadius:10,
        marginBottom:5,
        borderWidth:0,
        borderColor:'gray',
        borderBottomWidth:1,
        borderRightWidth:1,        
        width:300,
        alignSelf:'flex-end',
        marginRight:20,
    },
    dropDownBtnText:{
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        textAlign: 'right',
        paddingRight:10,
    },
    subHeaderText:{
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        fontSize:16,
        textAlign: 'center',
        alignSelf:'center',
        marginLeft:20,
        marginRight:20,
        marginTop:10,
        marginBottom:10
    },
    button:{
        backgroundColor:'#f4b642',
        height:50,
        borderRadius:15,
        alignSelf:'center',
        justifyContent: 'center',
        alignItems: 'center'

    },
    buttonEnter:{
        backgroundColor:'#fff',
        width:170,
        borderRadius:20,
        alignSelf:'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        color:'#000',
        fontWeight:'bold',
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        textAlign: 'center',

    },
    Footer:{
        height:20,
    },
});
