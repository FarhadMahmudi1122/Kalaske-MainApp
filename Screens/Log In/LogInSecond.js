import React from 'react';
import { bindActionCreators } from 'redux';

import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableNativeFeedback,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SmsListener from 'react-native-android-sms-listener'
import PushNotification from 'react-native-push-notification';
import { FormLabel, FormInput, Card, Button } from 'react-native-elements'
import { width, height, totalSize } from 'react-native-dimension';
import * as RegistrationActions from '../../actions/registrationActions';

function mapStateToProps({ registrationInfo }) {
  return {
    phoneNumber: registrationInfo.phoneNumber,
    isRegistered: registrationInfo.isRegistered,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(RegistrationActions, dispatch)
}

@connect(mapStateToProps,mapDispatchToProps)
export default class LogInSecond extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      codeNumber: null,
      sentCodeNumber: null,
      seconds:59,
      minutes:2,
      disableButtons:true,
      disableButtonsColor:'gray',      
      smsCount: 0,
    }
    
  }

  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      headerStyle: { height: 35 },
      drawerLockMode: 'locked-closed',
      headerLeft:null
    }
  };

  componentDidMount(){
    this._interval = setInterval(()=>{
      if(this.state.seconds!==0){
        this.setState({
          seconds:this.state.seconds-1,        
        })      
      }else if(this.state.minutes===2 && this.state.seconds===0){
        this.setState({
          seconds:59,
          minutes:1,
        })    
      }else if(this.state.minutes===1 && this.state.seconds===0){
        this.setState({
          seconds:59,
          minutes:0,
        })    
      }else if(this.state.minutes===0 && this.state.seconds===0){
        this.setState({
          disableButtons:false,
          disableButtonsColor:'blue'
        });
        clearInterval(this._interval)
      }
    },1000
    )
  }

  componentDidUpdate(){
    if(this.props.isRegistered){
      console.log('isRegistered from second log in..........:',this.props.isRegistered)
      this.props.navigation.replace('LogInThird');
    }
  }

  subscription = SmsListener.addListener(message => { 
    let verificationCode = message.body.match(/([\d]{4})/)
    let verificationProof = message.body.slice(0,12)
    if(verificationCode!==null&&verificationProof==='کد فعالسازی:'){
       this.setState({codeNumber:verificationCode[0]});
       this.fetchCodNumber(this.state.codeNumber);
       setTimeout(() => {this._AutoGoToThirdLogIn()},2000);      
       if(this.props.isRegistered){subscription.remove()}
    } else {
      return null
    }
  });

  _AutoGoToThirdLogIn(){
    this.props.navigation.replace('LogInThird')
  }

  // Dispatch Functions
  setRegistrationComplete() {
    this.props.setRegistrationComplete();
  }
    
  fetchCodNumber(codeNumber) {
    fetch(`http://rafshadow.ir/OperatorPinResponse?phone=${this.props.phoneNumber}&serviceId=608&pin=${codeNumber}`)
      .then((response) => response.json())
      .then((res) => {
        this.setState({
          sentCodeNumber: res
        });
        console.log('Delivered Pin From RafShadow :', res);
        if (res === 'ثبت نام کاربر با موفقیت انجام شد') {
          this.setRegistrationComplete()
        }
      });
  }

  _handleValidation() {
    const {codeNumber} = this.state
    if (codeNumber !== null) {
      if (codeNumber === ''|| codeNumber.length!==4) {
        alert('کد وارد شده صحیح نیست');
        return false
      } else {
        return true
      }
    } else {
      return true
    }
  }

//......................Handle Responsive Style......................

  _handleHeaderFontSize(){
    if(height(100)<600){
        return(12)
    }else{
        return(16)
    }
  }

  _handleButtonWidth(){
    if(height(100)<600){
      return(70)
    }else{
        return(100)
    }
  }

  _handleButtonTextFontSize(){
    if(height(100)<600){
      return(12)
    }else{
        return(16)
    }
  }

  _handleEnterTextFontSize(){
    if(height(100)<600){
      return(10)
    }else{
        return(12)
    }
  }


  _handleHeaderConditionsFontSize(){
    if(height(100)<600){
      return(12)
    }else{
        return(16)
    }
  }

  _handleSubHeaderConditionsFontSize(){
    if(height(100)<600){
      return(10)
    }else{
        return(14)
    }
  }

  render() {
    
    const { codeNumber,minutes,seconds,disableButtons,disableButtonsColor} = this.state;
    const { phoneNumber } = this.props;
    const zeroNumberForTimer = seconds<10 ? 0 : ''
    
    return (
      <ScrollView style={[styles.container]}>
        <View style={[styles.header]}>
          <Image
            style={[styles.headerImage]}
            source={require('../../Assets/Images/Login2.jpg')}
          />
          <Text style={[styles.headerText,{
            fontSize :this._handleHeaderFontSize()
          }]}>کد ارسال شده را در مستطیل زیر وارد کنید</Text>
          <Text style={[styles.subHeaderText,{
            fontSize :this._handleHeaderFontSize()
          }]}> شرایط استفاده از برنامه را مطالعه کنید و چنانچه موافقید، ادامه دهید.  
               </Text>
        </View>
        <View>
          <FormInput
            maxLength={4}
            underlineColorAndroid={'#bcbcbc'}
            onChangeText={(codeNumber) => this.setState({ codeNumber })}
            keyboardType='numeric'
            inputStyle={{ textAlign: 'center' }}
            placeholder={'کد چهار رقمی را وارد کنید'}
            value={this.state.codeNumber}
            />
            <View style={[styles.timerView]}>
              <Text style={styles.timerText}>{`0${minutes}:${zeroNumberForTimer}${seconds}`}</Text>
            </View>
        </View>
        <View style={{ alignSelf: 'center' }}>
          <TouchableOpacity
            onPress={() => {
              this._handleValidation()
              this.fetchCodNumber(codeNumber);
              if(this.props.isRegistered){this.props.navigation.replace('LogInThird');}
            }}

            style={[styles.button,{
              width:this._handleButtonWidth()
            }]}
          >
            <Text style={[styles.buttonText,{
              fontSize:this._handleButtonTextFontSize()
            }]}>ارسال</Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={disableButtons}
            onPress={() => this.props.navigation.replace('LogInFirst')}
            style={[styles.buttonEnter]}
          >
            <Text
              style={[styles.buttonText, {
                fontSize: this._handleEnterTextFontSize(),
                color: disableButtonsColor,
                borderBottomWidth: 1
              }]}
            >
              دوباره امتحان کنید
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={disableButtons}
            onPress={() => this.props.navigation.replace('Home')}
            style={[styles.buttonEnter, { width:300 }]}
          >
            <Text
              style={[styles.buttonText, {
                fontSize: this._handleEnterTextFontSize(),
                color: disableButtonsColor,
                borderBottomWidth: 1,
                
              }]}
            >
            ورود به نسخه آزمایشی
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.conditionView]}>
          <Text style={[styles.conditionHeaderText,{
            fontSize: this._handleHeaderConditionsFontSize(),       
          }]}>شرایط استفاده از برنامه</Text>
          <Text style={[styles.conditionSubHeaderText,{
            fontSize: this._handleSubHeaderConditionsFontSize(),
          }]}>«کالسکه» اپلیکیشنی است برای همراهی و راهنمایی در دوران بارداری، مرجع پزشکی نیست. اگر بیماری خاصی دارید، قبل به‌کارگیری مطالب با پزشک خود مشورت کنید.</Text>
        </View>
        <View style={[styles.Footer]}>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    alignItems: 'center',
    marginTop: 0,
  },
  headerImage: {
    width: width(100),
    height: height(30)
  },
  headerText: {
    marginTop: 5,
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',
    alignSelf: 'center',

  },
  subHeaderText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  timerView:{
    position:'absolute',
    marginTop:15,
    marginLeft:20
  },
  timerText:{
    color:'red',
    fontSize:20,
    fontWeight:'bold'
  },
  button: {
    backgroundColor: '#f4b642',
    height: 50,
    marginTop: 10,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'

  },
  buttonEnter: {
    backgroundColor: '#fff',
    width: 110,
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#000',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    textAlign: 'center',

  },
  conditionView: {
    paddingRight: 30,
    paddingLeft: 30,
    alignItems: 'flex-end'
  },
  conditionHeaderText: {
    color: '#ccc',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
  },
  conditionSubHeaderText: {
    color: '#999',
    fontWeight: 'bold',
    fontFamily: Platform.os === 'ios' ? 'IRANSansMobile' : 'iran_sans_mobile',
    marginBottom: 0
  },
  Footer: {
    height: 20,
  },

});
