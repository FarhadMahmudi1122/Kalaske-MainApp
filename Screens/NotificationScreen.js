import React from 'react';
import {Text, View, StyleSheet, ScrollView, Image, Platform} from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { width, height, totalSize } from 'react-native-dimension';
import { connect } from 'react-redux'
import NotificationList from './NotificationList'


function mapStateToProps({userInfo, registrationInfo}){
    return {
        registrationNotification: registrationInfo.registrationNotification,
    }
   }

@connect(mapStateToProps)
export default class AboutScreen extends React.Component {
    state = {
        value: 0
    };

    render(){
        const {registrationNotification} = this.props
        return (
            <View style={[styles.container]}>
                <View style={[styles.headerBar]}>
                    <View style={[styles.backIcons]}>
                        <Text
                            style={[styles.backText]}
                            onPress={() => { this.props.navigation.navigate('Home')}} >بازگشت</Text>
                        <MaterialIcons
                            style={[styles.backIcon]}
                            name={'keyboard-arrow-right'}
                            size={30}
                            onPress={() => { this.props.navigation.navigate('Home')}}/>
                    </View>
                </View>
                <Image style={[styles.hederImage]} source={require('../Assets/Images/NotifHeader.jpg')}/>
                <ScrollView style={styles.scrollViewContent}>
                    <NotificationList registrationNotification={registrationNotification}/>
                </ScrollView>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'#fff'
    },
    headerBar:{
        height:35,
        backgroundColor: 'white',
        elevation: 5,
    },
    backIcons:{
        alignSelf: 'flex-end',
        flex:1,
        flexDirection: 'row',
    },
    backText:{
        marginRight:-9,
        fontFamily:Platform.os === 'ios' ? 'IRANSansMobile':'iran_sans_mobile',
        color:'#000',
        fontSize:18,
        fontWeight:'bold',
    },
    backIcon:{
        color:'#000',
    },
    hederImage:{
        width:width(100),
        height:height(40),
        alignSelf:'center'
    },
    scrollViewContent:{
        marginTop:30,
    },

});